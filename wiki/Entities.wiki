#summary Entities description

= Introduction =

To provide the correct work of our application, we needed to design our own system of entities that are representating main objects taken part in the work of publisher-bookstore-customer system like publishers, shops, customers themselves and books, orders, authors , etc.


= Details =

==Book== 
Entity that makes sence to everything in this model. Has it's name, release date, connection to publisher that released it, lists of authors and genres.

==Customer, Bookstore, Publisher== 
The representation of the main interacting entities of our model. 

*Customer* can browse through the books in the bookstore, can choose books that he or she wants to buy. some books can be added to the "favourites" of this customer. customer can create orders on the books he or she wants to buy, even if these books are not released yet. bought books can be sold back by Customer to the store with their cost decrement.

*Bookstore* presents the info about which books are going to be released and gathers the info about what books are needed by the customers and send this information to the publishers. The bookstore recieves books orders from customers, works with statuses of the orders. Bookstore can choose which genre of books is the most profitable. Bookstore deals with payment to it's workers, can calculate the income of it's work.

*Publisher* works with authors - they inform publishers about the books that are going to be written, so publishers can inform bookstores about books that are going to be released. Publishers has the list of contracts with authors, which define the terms of publishing book of the concrete author (it includes the way and the size of payment). Also publisher can grant some bookstores with discounts on the books it selling them.

There is more abstract entity that unites Customers, Publishers and Bookstores - [https://code.google.com/p/sjc2014-giz/wiki/Subject Subject]


==Order== 
Entity that represents the interconnections between Customer and The Bookshop, and between Bookshop and Publisher. main goal of this entity - to bear the information about process of moving the books from publisher to the shop and from the shop to customer. It has links to the sender and to the recipient of the order, the list of books that are includede in it, info about the address that is used to send the books to the sender of the order, info about general cost of the order, about the date of it's creating. Also the pyment method of the order is described in it.

[https://code.google.com/p/sjc2014-giz/wiki/MakingOrder?ts=1406777872&updated=MakingOrder more about purchasing process...]