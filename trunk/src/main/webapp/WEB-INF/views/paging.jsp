<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- derivig url prefix for pages --%>
<c:set var="urlParams" value="?" />
<c:forEach items="${pageContext.request.parameterMap}" var="entry" varStatus="status">
    <c:forEach items="${entry.value}" var="value">
        <c:if test="${entry.key ne 'pageNum'}" >
            <c:set var="urlParams" value="${urlParams}&${entry.key}=${value}" />
        </c:if>
    </c:forEach>
</c:forEach>

<%--pages --%>
<div>
    <c:if test="${pageCount>0}">
        <c:forEach begin="1" end="${pageCount}" varStatus="page" >
            <c:if test="${pageNum eq page.index}">
                ${page.index}
            </c:if>
            <c:if test="${pageNum ne page.index}">
                <a href="<c:out value='${urlParams}&pageNum=${page.index}'/> "> ${page.index} </a>
            </c:if>
        </c:forEach>
    </c:if>
</div>