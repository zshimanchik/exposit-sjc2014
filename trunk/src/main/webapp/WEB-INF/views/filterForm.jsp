<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<style type="text/css">
    .error {
        color: red;
    }
</style>

<div id="filtering" style="padding: 20px; margin-bottom: 15px; border: 1px solid black;">
    <form:form  action="" method="GET" commandName="BooksFilter" modelAttribute="filter">
        <p style="font-weight: bold">filtering:</p>
        <table>
            <tr valign="top">
                <td>
                    <p>
                        <form:label path="name">name</form:label>
                        <form:input path="name" cssClass="form-control"/>
                    </p>
                    <p>
                        <form:label path="minPrice">minimal price</form:label>
                        <form:input path="minPrice" cssClass="form-control double" />
                    </p>
                    <p>
                        <form:label path="maxPrice">maximum price</form:label>
                        <form:input path="maxPrice" cssClass="form-control double" />
                    </p>
                </td>
                <td>
                    <form:label path="genres">Genres:</form:label>
                    <form:select path="genres" cssClass="form-control" size="8">
                        <form:option value="">Nothing</form:option>
                        <form:options items="${allGenres}" />
                    </form:select>
                </td>
                <td>
                    <form:label path="authors">Authors:</form:label>
                    <form:select path="authors" cssClass="form-control" size="8" >
                        <form:option value="">Nothing</form:option>
                        <form:options items="${allAuthors}" />
                    </form:select>
                </td>
                <c:if test="${empty publisher}" >
                    <td>
                        <form:label path="publishers">Publishers:</form:label>
                        <form:select path="publishers" cssClass="form-control" size="8" >
                            <form:option value="">Nothing</form:option>
                            <form:options items="${allPublishers}" />
                        </form:select>
                    </td>
                </c:if>

                <c:if test="${empty shop and empty publisher}">
                    <td>
                        <form:label path="shops">Shops:</form:label>
                        <form:select path="shops" cssClass="form-control" size="8" >
                            <form:option value="">Nothing</form:option>
                            <form:options items="${allShops}" />
                        </form:select>
                    </td>
                </c:if>
            </tr>
        </table>
        <input class="btn btn-default form-control" type="submit" value="filter" onclick="" />
    </form:form>
</div>

<script>
    jQuery.validator.setDefaults({
        success: "valid"
    });
    $( "#filter" ).validate({
        rules: {
            name : {
                minlength : 3,
                maxlength : 10
            },
            minPrice: {
                number: true
            },
            maxPrice: {
                number: true
            }
        }

    });
</script>