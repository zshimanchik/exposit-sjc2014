<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="form-group col-lg-4">
    <h3>Login</h3><br>
    <c:if test="${not empty error}"><font color="red">${error}</font></c:if>
    <c:if test="${not empty message}"><font color="#5f9ea0">${message}</font></c:if>
    <form id="loginForm" action="<c:url value='/j_spring_security_check'/>" method="POST">

        <p><input type="email" name="username" placeholder="email" class="form-control"/></p>
        <p></p><input type="password" name="password" placeholder="password" class="form-control"/></p>
        <p></p><input type="submit" value="login" class="btn btn-default form-control" /></p>

    </form>
</div>