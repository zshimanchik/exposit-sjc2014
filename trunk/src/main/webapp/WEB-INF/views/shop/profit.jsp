<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<div class="row">
    <h2>Profit of bookstore ${account.subject.name}</h2>

    <div class="col-lg-8">
        <h3>Enter period: </h3>
        <div class="row">
            <div class="col-lg-4">
                <input id="startDate" type="date" min="${min}" max="${max}" value="${min}" class="form-control">
            </div>
            <div class="col-lg-4">
                <input id="endDate" type="date" min="${min}" max="${max}" value="${max}" class="form-control">
            </div>
        </div>
        <br/>
        <input type="radio" name="date" id="2" checked> <label for="2">users dates</label>
        <br/>
        <input type="radio" name="date" id="1"> <label for="1">from first order</label>
        <br/>
        <input type="radio" name="date" id="0"> <label for="0">all profit</label>
        <br/><br/>
        <div class="col-lg-4 row pull-left">
            <a href="#" onclick="showProfit()" class="btn btn-primary form-control" role="button">show</a>
        </div>
    </div>

    <div class="col-lg-4" id="hide" hidden >
        <h3>Profit</h3>
        <i>from <strong id="from"></strong> to <strong id="to"></strong></i>
        <br/><br/>
        <p id="prof"></p>
    </div>

</div>
<script>
    $(document).ready(function(){
        $('#startDate').prop('disabled', false);
        $('#endDate').prop('disabled', false);
        $('input[name=date]').change(function() {
            var state = this.id;
            if(state == 2) {
                $('#startDate').prop('disabled', false);
                $('#endDate').prop('disabled', false);
            }
            else if(state == 1) {
                $('#startDate').val("${min}");
                $('#startDate').prop('disabled', true);
                $('#endDate').prop('disabled', false);
            } else {
                $('#startDate').val("${min}");
                $('#endDate').val("${max}");
                $('#startDate').prop('disabled', true);
                $('#endDate').prop('disabled', true);
            }
        });
    });

    function showProfit() {
        $.post(
            "<c:url value='/shop/profit/show'/>",
            {
               startDate: $('#startDate').val(),
               endDate  : $('#endDate').val()
            }
        ).success(function(data) {
            console.log('success', data);
            $("#prof").html(data);
            $("#from").html($('#startDate').val());
            $("#to").html($('#endDate').val());
            $("#hide").prop('hidden', false);
        }).error(function(data) {
            console.log('fail', data);
        });
    }
</script>
