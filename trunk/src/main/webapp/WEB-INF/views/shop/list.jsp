<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3>Stores</h3>

<br/><br/>
<table border="1" cellpadding="10" cellspacing="0" class="table table-striped table-bordered">
    <tr>
        <th>ID</th>
        <th>Name</th>
    </tr>

    <c:forEach items="${shops}" var="shop">
        <c:url value="/shop/${shop.id}" var="shopUrl"/>
        <tr>
            <td>${shop.id}</td>
            <td><a href="${shopUrl}">${shop.name}</a></td>
        </tr>
    </c:forEach>
</table>