<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="principal" value="${pageContext.request.userPrincipal.principal}" />
<c:set value="${principal.account}" var="account" />

<c:if test="${not empty shop}" >
    <h3>BookStore "${shop.name}" books</h3>
</c:if>

<c:url value="/cart/add" var="cartUrl"/>

<script type="text/javascript" >
function addBook(bookId, shopId, count) {

	var json = { book: { id: bookId }, seller: {id: shopId }, count: count };
	$.ajax({
        url: "${cartUrl}",
        data: JSON.stringify(json),
        type: "POST",
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function(obj) {
            $('#b' + bookId + 'sh' + shopId).html('In cart');
            $('#cart').html(obj);
        },
        error: function(){
            alert('failure');
        }
    })
}

function viewChangePrice(bookId, shopId) {
    $('#b' + bookId + 'sh' + shopId).prop('hidden', false);
    $('#b' + bookId + 'sh' + shopId + 'cl').prop('hidden', true);
}

function changePrice(bookId, shopId) {
    $.post("<c:url value='/shop/book/price/change'/>",{
       price: $('#b' + bookId + 'sh' + shopId + 'pr').val()
       , bookId: bookId
       , shopId: shopId
    }).success(function(price) {
        $('#b' + bookId + 'sh' + shopId).prop('hidden', true);
        $('#b' + bookId + 'sh' + shopId + 'cl').prop('hidden', false).html(price);
    }).error(function() {
       alert("error");
    });
}

</script>

<br/><br/>

<%--main--%>
<table border="1" cellpadding="10" cellspacing="0" class="table table-striped table-bordered">
    <tr>
        <th>Name</th>
        <th>Genres</th>
        <th>Authors</th>
        <th>Publisher</th>
        <c:if test="${empty shop}" ><th>Shop</th></c:if>
        <th>in stock</th>
        <th class="col-lg-2">Buy it</th>
    </tr>

    <c:forEach items="${books}" var="bookInShop">
        <c:set var="book" value="${bookInShop.book}" />
        <tr>
        	<c:url value="/book/${book.id}" var="bookUrl"/>

            <td><a href="${bookUrl}">${book.name}</a></td>

            <td>
                <c:forEach items="${book.genreList}" var="genre" varStatus="status">
                    <a href="?genres=${genre}">${genre}</a><c:if test="${!status.last}">,</c:if>
                    <br>
                </c:forEach>
            </td>

            <td>
                <c:forEach items="${book.authorList}" var="author" varStatus="status">
                    <a href="?authors=${author}">${author}</a><c:if test="${!status.last}">,</c:if>
                    <br>
                </c:forEach>
            </td>

            <td>
                <a href="?publishers=${book.publisher.name}">${book.publisher.name}</a>
            </td>

            <c:if test="${empty shop}" >
                <td>
                    <a href="?shops=${bookInShop.shop.name}">${bookInShop.shop.name}</a>
                </td>
            </c:if>

            <td>${bookInShop.count}</td>

            <td>
                <c:choose>
                    <c:when test="${account.subject.type.value == 'CUSTOMER'}">
                        <p align="center">${bookInShop.sellPrice}</p>
                        <c:if test="${bookInShop.count <= 0}">
                            <div align="center"><i>pre-order</i></div>
                        </c:if>
                        <div id="b${book.id}sh${bookInShop.shop.id}" align="center">
                            <input type="button" class="btn btn-link" onclick="addBook(${book.id}, ${bookInShop.shop.id}, 1)" value="Add to cart">
                        </div>
                    </c:when>
                    <c:when test="${bookInShop.shop.id == account.subject.id}">
                        <p onclick="viewChangePrice(${book.id}, ${bookInShop.shop.id})" id="b${book.id}sh${bookInShop.shop.id}cl">${bookInShop.sellPrice}</p>
                        <div id="b${book.id}sh${bookInShop.shop.id}" hidden class="col-lg-12">
                            <input type="text" id="b${book.id}sh${bookInShop.shop.id}pr" class="form-control" value="${bookInShop.sellPrice}">
                            <br/>
                            <a href="#" class="btn btn-default form-control" onclick="changePrice(${book.id}, ${bookInShop.shop.id})" role="button">change</a>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <p>${bookInShop.sellPrice}</p>
                    </c:otherwise>
                </c:choose>


            </td>
        </tr>
    </c:forEach>
</table>