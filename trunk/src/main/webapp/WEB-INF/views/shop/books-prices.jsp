<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="form-group col-lg-12">
    <h2>Set books prices</h2>
    <br/>
    <c:url value="/shop/books/price" var="paymentUrl" />
    <form:form action="${paymentUrl}" modelAttribute="books" method="POST">
        <table border=1 class="table table-striped table-bordered">
            <tr>
                <th>book id</th>
                <th>name</th>
                <th>authors</th>
                <th>released</th>
                <th>count</th>
                <th>set sell price</th>
            </tr>
            <c:forEach items="${books.bookInShopList}" var="bookInShop" varStatus="status">
                <tr valign="center" id="${bookInShop.book.id}">
                    <td>
                        <form:hidden path="bookInShopList[${status.index}].book.id" value="${bookInShop.book.id}"/>
                        <c:out value="${bookInShop.book.id}"/>
                    </td>
                    <td><a href='#'><c:out value="${bookInShop.book.name}" /></a></td>
                    <td><c:out value="${bookInShop.book.authorList}" /></td>
                    <td><c:out value="${bookInShop.book.isReleased}"/></td>
                    <td>
                        <form:hidden path="bookInShopList[${status.index}].count" value="${bookInShop.count}"/>
                        <c:out value="${bookInShop.count}"/>
                    </td>
                    <td><form:input path="bookInShopList[${status.index}].sellPrice" class="form-control" value="${bookInShop.sellPrice}"/></td>
                </tr>
            </c:forEach>
        </table>
        <input type="submit" value="Save" class="btn btn-success"/>
    </form:form>
</div>