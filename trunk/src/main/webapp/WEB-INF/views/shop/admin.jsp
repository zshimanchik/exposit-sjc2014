<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="principal" value="${pageContext.request.userPrincipal.principal}" />
<c:set value="${principal.account.subject}" var="shop" />

<h1>BookStore ${shop.name}</h1>
<h3>Admin page</h3>

<a href="<c:url value='/order/incoming'/> ">Incoming orders</a>
<br/>
<a href="<c:url value='/order/outgoing'/> ">Outgoing orders</a>
<br/>
<a href="<c:url value='/order/archive/outgoing'/>">Archive of completed outgoing orders</a>
<br/>
<a href="<c:url value='/order/archive/incoming'/>">Archive of completed incoming orders</a>
<br/>
<a href="<c:url value='/shop/${shop.id}/books'/>">Books in store</a>
<br/>
<a href="<c:url value='/shop/admin/genres/profitable'/>">Profitable genres</a>
<br/>
<a href="<c:url value='/shop/employees'/>">Employees</a>
<br/>
<a href="<c:url value='/shop/profit'/>">Profit</a>
<br/>
