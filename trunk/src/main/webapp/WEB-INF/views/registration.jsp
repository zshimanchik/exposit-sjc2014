<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="form-group col-lg-4">
    <h3>Registration</h3>

    <form:form action="registration" commandName="account" modelAttribute="account" method="POST">

        <form:input type="email" path="email" placeholder="email" class="form-control"/> <form:errors path="email"/> <br/>
        <form:input type="password" path="password" placeholder="password" class="form-control"/> <form:errors path="password"/> <br/>
        <form:input path="firstName" placeholder="First Name" class="form-control"/> <form:errors path="firstName"/> <br/>
        <form:input path="lastName" placeholder="Last Name" class="form-control"/> <form:errors path="lastName"/> <br/>


        <input type="submit" value="Save" class="btn btn-success"/>
        <input type="reset" value="Cancel" class="btn btn-danger"/>

    </form:form>

</div>