<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="principal" value="${pageContext.request.userPrincipal.principal}" />
<h1>Genres</h1>

<ul style="list-style: none">
<c:forEach items="${genres}" var="genre">
    <li><a href="<c:url value="/shop/books?genres=${genre}&shops=${principal.account.subject.name}"/>">${genre}</a></li>
</c:forEach>
</ul>
