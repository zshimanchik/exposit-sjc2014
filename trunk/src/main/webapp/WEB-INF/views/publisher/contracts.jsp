<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3>Publisher "${publisher.name}" contracts</h3>

<br/><br/>
<table border="1" cellpadding="10" cellspacing="0" class="table table-striped table-bordered">
    <tr>
        <th>Name</th>
        <th>Release date</th>
        <th>Authors</th>
        <th>Genres</th>
        <th>sell price</th>
        <th>is fixed tax</th>
        <th>tax value</th>
    </tr>

    <c:forEach items="${contracts}" var="contract">
        <tr>
            <td>${contract.book.name}</td>
            <td>${contract.book.releaseDate}</td>
            <td>${contract.book.authorList}</td>
            <td>${contract.book.genreList}</td>
            <td>${contract.sellPrice}</td>
            <td>${contract.isFixedTax}</td>
            <td>${contract.taxValue}</td>
        </tr>
    </c:forEach>
</table>