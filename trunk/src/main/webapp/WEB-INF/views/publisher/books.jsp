<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="principal" value="${pageContext.request.userPrincipal.principal}" />
<c:set value="${principal.account}" var="account" />

<c:if test="${!empty publisher}" >
    <h3>Publisher "${publisher.name}" books</h3>
</c:if>

<c:url value="/cart/add" var="cartUrl"/>

<script type="text/javascript" >
    function addBook(bookId, publisherId, count) {

        var json = { book: {id: bookId}, seller: {id:publisherId }, count: count};
        $.ajax({
            url: "${cartUrl}",
            data: JSON.stringify(json),
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function(obj) {
                $('#' + bookId).html('In cart');
                $('#cart').html(obj);
            },
            error: function(){
                alert('failure');
            }
        })
    }

</script>


<%--main--%>
<table border="1" cellpadding="10" cellspacing="0" class="table table-striped table-bordered">
    <tr>
        <th>Name</th>
        <th>Release date</th>
        <th>Released</th>
        <th>Genres</th>
        <th>Authors</th>
        <c:if test="${empty publisher}" ><th>Publisher</th></c:if>
        <th>sell price</th>
    </tr>

    <c:forEach items="${contracts}" var="contract">
        <c:set var="book" value="${contract.book}" />
        <tr>
            <td>
                <a href="<c:url value='/book/${book.id}'/>">${book.name}</a>
            </td>

            <td>
                ${book.releaseDate}
            </td>

            <td>
                ${book.isReleased}
            </td>

            <td>
                <c:forEach items="${book.genreList}" var="genre" varStatus="status">
                    <a href="?genres=${genre}">${genre}</a><c:if test="${!status.last}">,</c:if>
                    <br>
                </c:forEach>
            </td>

            <td>
                <c:forEach items="${book.authorList}" var="author" varStatus="status">
                    <a href="?authors=${author}">${author}</a><c:if test="${!status.last}">,</c:if>
                    <br>
                </c:forEach>
            </td>

            <c:if test="${empty publisher}" >
                <td>
                    <a href="?publishers=${book.publisher.name}">${book.publisher.name}</a>
                </td>
            </c:if>

            <td>
                <p align="center">${contract.sellPrice}</p>
                <c:if test="${account.subject.type.value == 'SHOP'}" var="acc" scope="session">
                    <div id="${book.id}" align="center">
                        <input type="button" class="btn btn-link" onclick="addBook(${book.id}, ${book.publisher.id}, 1)" value="Add to cart">
                    </div>
                </c:if>
            </td>

        </tr>
    </c:forEach>
</table>