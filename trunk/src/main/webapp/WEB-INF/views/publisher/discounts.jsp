<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
    function changeClick(publisherId, clientId) {
        var idPostfix = 'p'+publisherId+'c'+clientId;
        var value = $('#' + idPostfix+' div').html();
        $('#' + idPostfix).html(
                "<input id='inp"+idPostfix+"' value='"+value+"'/> " +
                "<input type=button value='change' onclick='change("+publisherId+","+clientId+")' /> "
        );
    }
    function change(publisherId, clientId) {
        var value = $('#inpp'+publisherId+'c'+clientId).val();
        $.post(
                "<c:url value='/publisher/ajax/discountChange'/>",
                { publisherId: publisherId, clientId: clientId, value: value}
        ).success(
                function() {
                    $('#p'+publisherId+'c'+clientId).html(
                                    "<div onclick='changeClick("+publisherId+","+clientId+")'>"+value+"</div>"
                    );
                }
        ).error(
                function() {
                    alert("error");
                }
        );
    }
</script>

<h3>Publisher "${publisher.name}" discounts</h3>

<table border="1" cellpadding="10" cellspacing="0" class="table table-striped table-bordered">
    <tr>
        <th>client</th>
        <th>discount (click to change)</th>
        <th>spend money</th>
    </tr>
    <c:forEach items="${discounts}" var="discount">
        <tr>
            <td>${discount.client.name}</td>
            <td id="p${discount.publisher.id}c${discount.client.id}">
                <div onclick="changeClick(${discount.publisher.id},${discount.client.id})">${discount.value}</div>
            </td>
            <td>${discount.spendMoney}</td>
        </tr>
    </c:forEach>
</table>