<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3>Publishers</h3>

<br/><br/>
<table border="1" cellpadding="10" cellspacing="0" class="table table-striped table-bordered">
    <tr>
        <th>ID</th>
        <th>Name</th>
    </tr>

    <c:forEach items="${publishers}" var="publisher">
        <c:url value="/publisher/${publisher.id}" var="publisherUrl"/>
        <tr>
            <td>${publisher.id}</td>
            <td><a href="${publisherUrl}">${publisher.name}</a></td>
        </tr>
    </c:forEach>
</table>