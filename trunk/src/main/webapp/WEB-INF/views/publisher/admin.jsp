<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="principal" value="${pageContext.request.userPrincipal.principal}" />
<c:set value="${principal.account.subject}" var="publisher" />

<h1>Publisher ${publisher.name}</h1>
<h3>Admin page</h3>

<a href="<c:url value='/order/incoming'/> ">Incoming orders</a>
<br/>
<a href="<c:url value='/order/archive/incoming'/>">Archive of completed incoming orders</a>
<br/>
<a href="<c:url value='/publisher/${publisher.id}/books'/>">Books</a>
<br/>
<a href="<c:url value='/publisher/contracts'/>">contracts</a>
<br/>
<a href="<c:url value='/publisher/employees'/>">Employees</a>
<br>
<a href="<c:url value='/publisher/discounts'/>">Discounts</a>
<br>
<a href="<c:url value='/publisher/addBook'/>">add book</a>