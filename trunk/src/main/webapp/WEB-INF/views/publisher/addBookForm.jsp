<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript" >
    function addGenre() {
        var genreName = $("#newGenre").val();

        $.post(
                "<c:url value='/publisher/ajax/addBook/addGenre' />",
                { genreName: genreName }
        ).success(
                function() {
                    alert("ok. reload page to view result");

                }
        ).error(
                function() {
                    alert("error");
                }
        );
    }

    function addAuthor() {
        var authorName =  $("#newAuthor").val();

        $.post(
                "<c:url value='/publisher/ajax/addBook/addAuthor'/>",
                { authorName: authorName }
        ).success(
                function() {
                    alert("ok. reload page to view result");
                }
        ).error(
                function() {
                    alert("error");
                }
        );
    }
</script>



<h3>Add book for publisher "${publisher.name}"</h3>
<br/><br/>

<div>
<c:url value='/publisher/addBook' var="actionUrl"/>
<form:form action="${actionUrl}" commandName="ContractWithAuthors" modelAttribute="contract" method="POST">

    <form:label path="book.name">Book name</form:label>
    <form:input path="book.name" placeholder="name" class="form-control"/> <form:errors path="book.name"/> <br/>

    <form:label path="book.releaseDate">Release date(format: dd/MM/yyyy):</form:label>
    <form:input cssClass="form-control" type="date" path="book.releaseDate" /><form:errors path="book.releaseDate"/><br /><br>

    <div class="row" >
        <div class="container form-group col-lg-1" style="width : 302px; ">
            <form:label path="book.genreList" cssClass="caption">Genres:</form:label>
            <form:select path="book.genreList" items="${genres}" multiple="true" size="8" cssClass="form-control" />
            <form:errors path="book.genreList" />

            <input id="newGenre" type="text" placeholder="new genre"   class="form-control"/>
            <input type="button" value="add genre"  class="btn btn-default form-control" onclick="addGenre()" />
        </div>

        <div class="container form-group col-md-1" style="width: 302px;">
            <form:label path="book.authorList" cssClass="caption">Authors:</form:label>
            <form:select path="book.authorList" items="${authors}" multiple="true" size="8" cssClass="form-control" />
            <form:errors path="book.authorList" />

            <input id="newAuthor" type="text" placeholder="new author"   class="form-control"/>
            <input type="button" value="add author" class="btn btn-default form-control" onclick="addAuthor()" />
        </div>


        <div class="container form-group col-sm-1" style="width: 202px; ">
            <p class="caption " style="font-weight: bold">Contract:</p>
            <form:checkbox path="isFixedTax" label="Is fixed tax" /> <form:errors path="isFixedTax"/><br />

            <form:label path="taxValue" >Tax Value:</form:label>
            <form:input path="taxValue" placeholder="tax value" class="form-control"/><form:errors path="taxValue"/><br/>

            <form:label path="sellPrice" >Sell price:</form:label>
            <form:input path="sellPrice" placeholder="sell price" class="form-control"/> <form:errors path="sellPrice"/> <br/>
        </div>
    </div>
    <input type="submit" value="add" class="btn btn-default form-control" />

</form:form>
</div>