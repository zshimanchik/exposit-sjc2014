<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3>"${employer.name}" employees</h3>

<br/><br/>
<table border="1" cellpadding="10" cellspacing="0" class="table table-striped table-bordered">
    <tr>
        <th>account id</th>
        <th>email</th>
        <th>first name</th>
        <th>last name</th>
    </tr>

    <c:if test="${not empty employees}">
        <c:forEach items="${employees}" var="employee">
            <tr>
                <td>${employee.id}</td>
                <td>${employee.email}</td>
                <td>${employee.firstName}</td>
                <td>${employee.lastName}</td>
            </tr>
        </c:forEach>
    </c:if>
    <c:if test="${empty employees}">
        <tr>
            <td colspan="4">There is no employees =(</td>
        </tr>
    </c:if>


</table>