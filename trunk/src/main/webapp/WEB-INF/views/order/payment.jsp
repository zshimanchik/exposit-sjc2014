<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="form-group col-lg-4">
    <h3>Payment method</h3>
    <c:url value="/order/payment" var="actionUrl" />
    <form:form action="${actionUrl}" commandName="payment" modelAttribute="payment" method="POST">

        <form:select path="paymentMethod" class="form-control">
            <c:forEach items="${paymentMethods}" var="method">
                <form:option value="${method.value}">${method.value}</form:option>
            </c:forEach>
        </form:select>
        <br/>
        <form:input path="userIdInService" placeholder="User ID in service" class="form-control"/>
        <form:errors path="userIdInService"/>
        <br/>
        <h3>Address</h3>
        <input type="text" name="address" placeholder="Address" class="form-control"/>
        <p>${message}</p>
        <br/>
        <a href="<c:url value='/cart/'/>" class="btn btn-primary" role="button">back</a>
        <input type="submit" value="confirm" class="btn btn-success"/>
    </form:form>
</div>