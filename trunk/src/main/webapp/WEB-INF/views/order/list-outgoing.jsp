<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript" >
    function cancelBook(orderId, bookId) {
        $.post("<c:url value='/order/ajax/cancelBookFromOutgoingOrder'/>",
           { orderId: orderId, bookId: bookId }
        ).success(function(orders) {
           console.info("returned orders");
           console.log(orders);
           $("#ord"+orderId).after(ordersToStr(orders)).remove();
        }).error(function() {
           alert("error");
        });
    }

    function cancelOrder(orderId) {
        $.post("<c:url value='/order/ajax/cancelOutgoingOrder'/>",
           { orderId: orderId }
        ).success(function(order) {
           console.info("returned order");
           console.info(order);
           var xz = $("#ord"+orderId);
           xz.after(orderToStr(order)).remove();
        }).error(function() {
           alert("error");
        });
    }
    function confirmOrder(orderId) {
        $.post("<c:url value='/order/ajax/confirmOutgoingOrder'/>",
                { orderId : orderId }
        ).success(
                function(order) {
                    console.info("returned order");
                    console.info(order);
                    $("#ord"+orderId).after(orderToStr(order)).remove();
                }
        ).error(
                function() {
                    alert("error");
                }
        );
    }


    function ordersToStr(orders) {
        var res = "";
        for (i in orders) {
            res += orderToStr(orders[i]);
        }
        return res;
    }

    function orderToStr(order) {
        var res = "<tr id='ord" + order.id + "'>" +
                "<td>" + order.id + "</td>" +
                "<td>" + order.sender.name + "</td>" +
                "<td>" + order.recipient.name + "</td>" +
                "<td>" + recordsToStr(order) + "</td>" +
                "<td>" + order.cost + "</td>" +
                "<td>" + order.address + "</td>" +
                "<td>" + paymentToStr(order.payment) + "</td>" +
                "<td>" + order.isPreorder + "</td>" +
                "<td>" + order.state + "</td>" +
                "<td>" + order.changeDate + "</td>" +
                "<td>" + deriveHandlersStr(order) +"</td>" +
                "</tr>";
        return res;
    }

    function recordsToStr(order) {
        var records = order.records;
        var res = '<table border=1 class="table table-striped table-bordered">'+
                '<thead>' +
                '<th>name</th>'+
                '<th>authors</th>'+
                '<th>released</th>'+
                '<th>count</th>'+
                '<th>book price</th>';

        if (canCancel(order))
            res += '<th>cancel</th>';

        res += '</thead>'+

                '<tbody>';

        <c:url value="/book/" var="bookBaseUrl"/>
        for (var i in records) {
            var record = records[i];
            var book = record.book;

            res += '<tr valign="center" id="ord' + order.id + 'bk' + book.id + '">' +
                    '<td><a href="${bookBaseUrl}'+book.id+'">'+book.name+'</a></td>' +
                    '<td>'+book.authorList+'</td>' +
                    '<td>'+book.isReleased+'</td>' +
                    '<td>'+record.count+'</td>' +
                    '<td>'+record.bookPrice;
            if (canCancel(order.state))
                res += '<input type="button" value="cancel" onclick="cancelBook('+order.id+','+book.id+')" />';
            res += '</td></tr>';
        }

        res += '</tbody>' +
                '</table>';
        return res;
    }

    function paymentToStr(payment) {
        var res = '<table border="1" class="table table-striped table-bordered">'+
                '<tr>'+
                '<td>id</td>'+
                '<td>'+payment.id+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td>method</td>'+
                '<td>'+payment.paymentMethod+'</td>'+
                '</tr>'+
                '<tr>'+
                '<td>user id in service</td>'+
                '<td>'+payment.userIdInService+'</td>'+
                '</tr>'+
                '</table>';
        return res;
    }

    function deriveHandlersStr(order) {
        var res = "";
        if (order.state=='PAYED')
            res = '<input type="button" value="accept" onclick="confirmOrder(' + order.id + ')" />';
        if (order.state=='RECIPIENT_REJECTED')
            res = '<input type="button" value="okay" onclick="confirmOrder(' + order.id + ')" />';

        if (canCancel(order.state))
            res += '<input type="button" value="cancel order" onclick="cancelOrder('+order.id+')" />';
        return res;
    }

    function canCancel(orderState) {
        return (orderState=='NEW' ||
                orderState=='WAITING_FOR_BOOK_RELEASES' ||
                orderState=='WAITING_FOR_RECIPIENT_FEASIBILITY' ||
                orderState=='WAITING_FOR_PAY');
    }


</script>


<h3>"${subject.name}" outgoing orders</h3>

<br/><br/>
<table border="1" cellpadding="10" cellspacing="0" class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Sender</th>
        <th>Recipient</th>
        <th>Books</th>
        <th>Cost</th>
        <th>Address</th>
        <th>Payment</th>
        <th>is pre order</th>
        <th>State</th>
        <th>Date</th>
        <th>Handle</th>
    </tr>

    <c:forEach items="${orders}" var="order">
        <c:if test="${order.state == 'NEW' or
            order.state == 'WAITING_FOR_BOOK_RELEASES' or
            order.state == 'WAITING_FOR_RECIPIENT_FEASIBILITY' or
            order.state == 'WAITING_FOR_PAY'}"
        var="canCancel" />

        <tr id="ord${order.id}">
            <td><c:out value="${order.id}" /></td>
            <td><c:out value="${order.sender.name}"/></td>
            <td><c:out value="${order.recipient.name}"/></td>
            <td>
                <table border=1 class="table table-striped table-bordered">
                    <tr>
                        <th>name</th>
                        <th>authors</th>
                        <th>released</th>
                        <th>count</th>
                        <th>book price</th>
                        <c:if test="${canCancel}"><th>cancel book</th></c:if>
                    </tr>
                    <c:forEach items="${order.records}" var="record">
                        <c:set var="book" value="${record.book}" />
                        <tr valign="center" id="ord${order.id}bk${book.id}">
                            <td><a href='<c:url value="/book/${book.id}"/>'><c:out value="${book.name}" /></a></td>
                            <td><c:out value="${book.authorList}" /></td>
                            <td><c:out value="${book.isReleased}"/> </td>
                            <td><c:out value="${record.count}"/></td>
                            <td><c:out value="${record.bookPrice}"/></td>
                            <c:if test="${canCancel}">
                                <td>
                                    <input type="button" value="cancel" onclick="cancelBook(${order.id},${book.id})" />
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                </table>
            </td>
            <td><c:out value="${order.cost}"/></td>
            <td><c:out value="${order.address}"/></td>
            <td>
                    <%--order info--%>
                <table border="1" class="table table-striped table-bordered">
                    <tr>
                        <td>id</td>
                        <td><c:out value="${order.payment.id}"/></td>
                    </tr>
                    <tr>
                        <td>method</td>
                        <td><c:out value="${order.payment.paymentMethod}" /></td>
                    </tr>
                    <tr>
                        <td>user id in service</td>
                        <td><c:out value="${order.payment.userIdInService}" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <c:out value="${order.isPreorder}"/>
            </td>
            <td id="ord${order.id}st">
                <c:out value="${order.state}"/>
            </td>
            <td>
                <c:out value="${order.changeDate}"/>
            </td>
            <td>
                <c:choose>
                    <c:when test="${order.state == 'PAYED' && subject.type == 'SHOP'}">
                        <input type="button" value="take books" onclick="confirmOrder(${order.id})"/>
                    </c:when>
                    <c:when test="${order.state == 'PAYED'}">
                        <input type="button" value="confirm" onclick="confirmOrder(${order.id})"/>
                    </c:when>
                    <c:when test="${order.state == 'RECIPIENT_REJECTED'}">
                        <input type="button" value="okay" onclick="confirmOrder(${order.id})"/>
                    </c:when>
                </c:choose>
                <c:if test="${canCancel}">
                    <input type="button" value="cancel order" onclick="cancelOrder(${order.id})" />
                </c:if>
            </td>

        </tr>
    </c:forEach>
</table>