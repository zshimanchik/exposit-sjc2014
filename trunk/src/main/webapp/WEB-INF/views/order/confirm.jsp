<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<c:set var="principal" value="${pageContext.request.userPrincipal.principal}" />
<c:set value="${principal.account}" var="account" />
<h3>Confirm order</h3>

<table border="1" class="table table-striped table-bordered">
    <tr>
        <th>Name</th>
        <th>Authors</th>
        <%--<th>Price</th>--%>
        <th>Count</th>
    </tr>
    <c:forEach items="${account.cart.books}" var="bookInCart">
        <c:set var="book" value="${bookInCart.book}" />
        <tr>
            <td>${book.name}</td>
            <td>${book.authorList}</td>
            <%--<td>${book.sellPrice}</td>--%>
            <td>${bookInCart.count}</td>
        </tr>
    </c:forEach>
</table>
<br/>
<p>Payment method: ${payment.paymentMethod.value}</p>
<br/>
<p>ID: ${payment.userIdInService}</p>
<br/>
<p>Address: ${address}</p>
<br/>
<a href="<c:url value='/order/payment'/>" class="btn btn-primary" role="button">back</a>
<a href="<c:url value='/order/new'/>" class="btn btn-success" role="button">Yes, I want to buy it!</a>


