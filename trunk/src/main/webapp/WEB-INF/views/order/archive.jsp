<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table border="1" cellpadding="10" cellspacing="0" class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Sender</th>
        <th>Recipient</th>
        <th>Books</th>
        <th>Cost</th>
        <th>Address</th>
        <th>Payment</th>
        <th>State</th>
        <th>Date</th>
    </tr>

    <c:forEach items="${orders}" var="order">
        <tr id="ord${order.id}">
            <td><c:out value="${order.id}" /></td>
            <td><c:out value="${order.sender.name}"/></td>
            <td><c:out value="${order.recipient.name}"/></td>
            <td>
                <table border=1 class="table table-striped table-bordered">
                    <tr>
                        <th>book id</th>
                        <th>name</th>
                        <th>authors</th>
                        <th>released</th>
                        <th>count</th>
                        <th>book price</th>
                    </tr>
                    <c:forEach items="${order.records}" var="record">
                        <c:set var="book" value="${record.book}" />
                        <tr valign="center" id="ord${order.id}bk${book.id}">
                            <td><c:out value="${book.id}"/></td>
                            <td><a href='#'><c:out value="${book.name}" /></a></td>
                            <td><c:out value="${book.authorList}" /></td>
                            <td><c:out value="${book.isReleased}"/> </td>
                            <td><c:out value="${record.count}"/></td>
                            <td><c:out value="${record.bookPrice}"/></td>
                        </tr>
                    </c:forEach>
                </table>
            </td>
            <td><c:out value="${order.cost}"/></td>
            <td><c:out value="${order.address}"/></td>
            <td>
                <%--order info--%>
                <table border="1" class="table table-striped table-bordered">
                    <tr>
                        <td>id</td>
                        <td><c:out value="${order.payment.id}"/></td>
                    </tr>
                    <tr>
                        <td>method</td>
                        <td><c:out value="${order.payment.paymentMethod}" /></td>
                    </tr>
                    <tr>
                        <td>user id in service</td>
                        <td><c:out value="${order.payment.userIdInService}" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <c:out value="${order.state}"/>
            </td>
            <td>
                <c:out value="${order.changeDate}"/>
            </td>

        </tr>
    </c:forEach>
</table>