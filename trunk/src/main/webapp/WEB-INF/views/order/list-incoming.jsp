<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<script type="text/javascript" >
    function rejectBook(orderId, bookId) {
        $.post(
                "<c:url value='/order/ajax/rejectBookFromIncomingOrder'/>",
                { orderId: orderId, bookId: bookId }
        ).success(
                function(orders) {
                    console.info("returned orders");
                    console.info(orders);
                    $("#ord"+orderId).after(ordersToStr(orders)).remove();
                }
        ).error(
                function() {
                    alert("error");
                }
        );
    }

    function rejectOrder(orderId) {
        $.post(
                "<c:url value='/order/ajax/rejectIncomingOrder'/>",
                { orderId: orderId }
        ).success(
                function(order) {
                    console.info("returned order");
                    console.info(order);
                    $("#ord"+orderId).after(orderToStr(order)).remove();
                }
        ).error(
                function() {
                    alert("error");
                }
        );
    }
    function confirmOrder(orderId) {
        $.post(
                "<c:url value='/order/ajax/confirmIncomingOrder'/>",
                { orderId : orderId }
        ).success(
                function(order) {
                    console.info("returned order");
                    console.info(order);
                    $("#ord"+orderId).after(orderToStr(order)).remove();
                }
        ).error(
                function() {
                    alert("error");
                }
        );
    }

    function ordersToStr(orders) {
        res = "";
        for (i in orders) {
            res += orderToStr(orders[i]);
        }
        return res;
    }

    function orderToStr(order) {
        var res = "<tr id='ord" + order.id + "'>" +
                "<td>" + order.id + "</td>" +
                "<td>" + order.sender.name + "</td>" +
                "<td>" + order.recipient.name + "</td>" +
                "<td>" + recordsToStr(order) + "</td>" +
                "<td>" + order.cost + "</td>" +
                "<td>" + order.address + "</td>" +
                "<td>" + paymentToStr(order.payment) + "</td>" +
                "<td>" + order.isPreorder + "</td>" +
                "<td>" + order.state + "</td>" +
                "<td>" + order.changeDate + "</td>" +
                "<td>" + deriveHandlersStr(order) +"</td>" +
              "</tr>";
        return res;
    }

    function recordsToStr(order) {
        var records = order.records;
        var res = '<table border=1 class="table table-striped table-bordered">'+
        '<thead>' +
        '<th>name</th>'+
        '<th>authors</th>'+
        '<th>released</th>'+
        '<th>count</th>'+
        '<th>book price</th>';

        if (canReject(order))
            res += '<th>reject</th>';

        res += '</thead>'+

        '<tbody>';

        <c:url value="/book/" var="bookBaseUrl"/>
        for (var i in records) {
            var record = records[i];
            var book = record.book;

            res += '<tr valign="center" id="ord' + order.id + 'bk' + book.id + '">' +
            '<td><a href="${bookBaseUrl}'+book.id+'">'+book.name+'</a></td>' +
            '<td>'+book.authorList+'</td>' +
            '<td>'+book.isReleased+'</td>' +
            '<td>'+record.count+'</td>' +
            '<td>'+record.bookPrice;
            if (canReject(order.state))
                res += '<input type="button" value="reject" onclick="rejectBook('+order.id+','+book.id+')" />';
            res += '</td></tr>';
        }

        res += '</tbody>' +
                '</table>';
        return res;
    }

    function paymentToStr(payment) {
        var res = '<table border="1" class="table table-striped table-bordered">'+
            '<tr>'+
                '<td>id</td>'+
                '<td>'+payment.id+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>method</td>'+
                '<td>'+payment.paymentMethod+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>user id in service</td>'+
                '<td>'+payment.userIdInService+'</td>'+
            '</tr>'+
        '</table>';
        return res;
    }

    function deriveHandlersStr(order) {
        var res =""
        if (order.state=='NEW')
            res = '<input type="button" value="accept" onclick="confirmOrder(' + order.id + ')" />';
        if (order.state=='WAITING_FOR_BOOK_RELEASES')
            res = '<input type="button" value="check book releases" onclick="confirmOrder(' + order.id + ')" />';
        if (order.state=='WAITING_FOR_RECIPIENT_FEASIBILITY')
            res = '<input type="button" value="check feasibility" onclick="confirmOrder(' + order.id + ')" />';
        if (order.state=='WAITING_FOR_PAY')
            res = '<input type="button" value="check user pay" onclick="confirmOrder(' + order.id + ')" />';
        if (order.state=='SENDER_CANCELED')
            res = '<input type="button" value="okay" onclick="confirmOrder(' + order.id + ')" />';

        if (canReject(order.state))
            res += '<input type="button" value="reject" onclick="rejectOrder(' + order.id + ')" />';
        return res;
    }

    function canReject(orderState) {
        return (orderState=='NEW' ||
                orderState=='WAITING_FOR_BOOK_RELEASES' ||
                orderState=='WAITING_FOR_RECIPIENT_FEASIBILITY');
    }
</script>

<h3>"${subject.name}" incoming orders</h3>

<br/><br/>
<table id="orderTable" border="1" cellpadding="10" cellspacing="0" class="table table-striped table-bordered">
    <thead>
        <th>Id</th>
        <th>Sender</th>
        <th>Recipient</th>
        <th>Books</th>
        <th>Cost</th>
        <th>Address</th>
        <th>Payment</th>
        <th>is pre order</th>
        <th>State</th>
        <th>Date</th>
        <th>Handle</th>
    </thead>

    <tbody>
    <c:forEach items="${orders}" var="order">
        <c:if test="${order.state=='NEW' or
                                order.state=='WAITING_FOR_BOOK_RELEASES' or
                                order.state=='WAITING_FOR_RECIPIENT_FEASIBILITY'}"
              var="canReject" />

        <tr id="ord${order.id}">
            <td><c:out value="${order.id}" /></td>
            <td><c:out value="${order.sender.name}"/></td>
            <td><c:out value="${order.recipient.name}"/></td>
            <td>
                <table border=1 class="table table-striped table-bordered">

                    <thead>
                        <th>name</th>
                        <th>authors</th>
                        <th>released</th>
                        <th>count</th>
                        <th>book price</th>
                        <c:if test="${canReject}"><th>reject book</th></c:if>
                    </thead>

                    <tbody>
                    <c:forEach items="${order.records}" var="record">
                        <c:set var="book" value="${record.book}" />
                        <tr valign="center" id="ord${order.id}bk${book.id}">
                            <td><a href='<c:url value="/book/${book.id}"/>'><c:out value="${book.name}" /></a></td>
                            <td><c:out value="${book.authorList}" /></td>
                            <td><c:out value="${book.isReleased}"/> </td>
                            <td><c:out value="${record.count}"/></td>
                            <td><c:out value="${record.bookPrice}"/></td>
                            <c:if test="${canReject}">
                                <td>
                                    <input type="button" value="reject" onclick="rejectBook(${order.id},${book.id})" />
                                </td>
                            </c:if>
                        </tr>
                    </c:forEach>
                    </tbody>

                </table>
            </td>
            <td><c:out value="${order.cost}"/></td>
            <td><c:out value="${order.address}"/></td>
            <td>
                    <%--order info--%>
                <table border="1" class="table table-striped table-bordered">
                    <tr>
                        <td>id</td>
                        <td><c:out value="${order.payment.id}"/></td>
                    </tr>
                    <tr>
                        <td>method</td>
                        <td><c:out value="${order.payment.paymentMethod}" /></td>
                    </tr>
                    <tr>
                        <td>user id in service</td>
                        <td><c:out value="${order.payment.userIdInService}" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <c:out value="${order.isPreorder}"/>
            </td>
            <td>
                <c:out value="${order.state}"/>
            </td>
            <td>
                <c:out value="${order.changeDate}"/>
            </td>
            <td>
                <%--confirming order--%>
                <c:choose>
                    <c:when test="${order.state=='NEW'}">
                        <input type="button" value="accept" onclick="confirmOrder(${order.id})" />
                    </c:when>
                    <c:when test="${order.state=='WAITING_FOR_BOOK_RELEASES'}">
                        <input type="button" value="check book releases" onclick="confirmOrder(${order.id})" />
                    </c:when>
                    <c:when test="${order.state=='WAITING_FOR_RECIPIENT_FEASIBILITY'}">
                        <input type="button" value="check feasibility" onclick="confirmOrder(${order.id})" />
                    </c:when>
                    <c:when test="${order.state=='WAITING_FOR_PAY'}">
                        <input type="button" value="check user pay" onclick="confirmOrder(${order.id})" />
                    </c:when>
                    <c:when test="${order.state=='SENDER_CANCELED'}">
                        <input type="button" value="okay" onclick="confirmOrder(${order.id})" />
                    </c:when>
                </c:choose>

                <c:if test="${canReject}">
                    <input type="button" value="reject" onclick="rejectOrder(${order.id})" />
                </c:if>

            </td>

        </tr>
    </c:forEach>
    </tbody>
</table>