<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="principal" value="${pageContext.request.userPrincipal.principal}" />
<h2>Hello, ${principal.account.firstName}!</h2>

<c:if test="${principal.account.subject.type.value == 'SHOP' || principal.account.subject.type.value == 'CUSTOMER'}">
    <a href="<c:url value='/order/outgoing' /> ">Outgoing orders</a>
</c:if>
<c:if test="${principal.account.subject.type.value == 'SHOP' || principal.account.subject.type.value == 'PUBLISHER'}">
    <a href="<c:url value='/order/incoming' /> ">Incoming orders</a>
</c:if>

<a href="<c:url value='/profile/favorites' /> ">Favorite books</a>

<table border="1"  class="table table-striped table-bordered">
    <tr><td>account id</td><td>${account.id}</td></tr>
    <tr><td>email</td><td>${account.email}</td></tr>
    <tr><td>first name</td><td>${account.firstName}</td></tr>
    <tr><td>last name</td><td>${account.lastName}</td></tr>
    <tr>
        <td>Subject</td>
        <td>
            <table border="1" class="table table-striped table-bordered">
                <tr><td>id</td><td>${account.subject.id}</td></tr>
                <tr><td>name</td><td>${account.subject.name}</td></tr>
                <tr><td>type</td><td>${account.subject.type}</td></tr>
            </table>
        </td>
    </tr>
    <tr><td>The sum spent on books</td><td>${spentSum}</td></tr>
</table>
