<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="principal" value="${pageContext.request.userPrincipal.principal}" />
<c:set value="${principal.account}" var="account" />

<h2>${account.firstName}'s cart!</h2>

<c:url value="/cart/delete" var="cartUrl"/>
<c:url value="/cart/add" var="cartCountUrl"/>

<table border="1" class="table table-striped table-bordered">
    <tr>
        <th>Name</th>
        <th>Authors</th>
        <th>Seller</th>
        <th>Count</th>
        <th></th>
    </tr>
    <c:forEach items="${account.cart.books}" var="bookInCart">
        <c:set var="book" value="${bookInCart.book}" />
        <tr>
            <td>${book.name}</td>
            <td>${book.authorList}</td>
            <td>${bookInCart.seller.name}</td>
            <td><input id="cnt${book.id}s${bookInCart.seller.id}" type="number" min="1" value="${bookInCart.count}" onchange="changeCount('cnt${book.id}s${bookInCart.seller.id}', ${book.id}, ${bookInCart.seller.id})"></td>
            <td><div id="${book.id}"><a href="#" onclick="deleteBook(${book.id})">remove</a></div></td>
        </tr>
    </c:forEach>

</table>
    <div id="order_price" class="pull-right" hidden>
        <br/><br/>
        <span id="price" class="pull-right">${price}</span> <strong class="pull-right">Price of order: </strong>
        <br/><br/>
        <a href="#" class="btn btn-primary" onclick="recalculate()" role="button">recalculate</a>
        <a href="<c:url value='/order/payment'/>" class="btn btn-primary" role="button">next</a>
    </div>
<div class="messages">"You don't have any book in shopping cart! So, add it!)"</div>
<script type="text/javascript" >
    $(document).ready(function() {
        if ("${fn:length(account.cart.books)}" > 0) {
            $('#order_price').prop('hidden', false);
            $('.messages').prop('hidden', true)
        }
    });

    function deleteBook(bookId) {

        var json = { id: bookId };
        $.ajax({
            url: "${cartUrl}",
            data: JSON.stringify(json),
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function(obj) {
                $('#' + bookId).html('removed');
                $('#cart').html('Cart (' + ${fn:length(account.cart.books) + 1} + ')');
            },
            error: function(){
                alert('failure');
            }
        })
    }

    function changeCount(controlId, bookId, sellerId) {
        var bookCount = $('#' + controlId).val();
        var json = { book: {id: bookId}, count: bookCount, seller: {id: sellerId } };
        $.ajax({
            url: "${cartCountUrl}",
            data: JSON.stringify(json),
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function(obj) {
            },
            error: function(){
                alert('failure');
            }
        })
    }

    function recalculate() {
        $.post("<c:url value='/cart/recalculate'/>")
            .success(function(data) {
                console.log('success', data);
                $('#price').html(data);

            }).error(function(data) {
                console.log('fail', data);
            });
    }

</script>