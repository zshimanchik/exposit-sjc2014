<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="principal" value="${pageContext.request.userPrincipal.principal}" />

<ul class="nav nav-pills nav-stacked">
    <c:if test="${empty principal}">
        <li><a href="<c:url value='/login'/>">Login</a></li>
        <li><a href="<c:url value='/registration'/>">Registration</a></li>
    </c:if>
    <c:if test="${not empty principal}">
        <c:if test="${principal.account.subject.type.value == 'SHOP'}">
            <li><a href="<c:url value="/shop/admin"/>">Admin</a></li>
        </c:if>
        <c:if test="${principal.account.subject.type.value == 'PUBLISHER'}">
            <li><a href="<c:url value="/publisher/admin"/>">Admin</a></li>
        </c:if>
        <li><a href="<c:url value='/j_spring_security_logout'/>">Logout</a></li>
    </c:if>
</ul>