<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="principal" value="${pageContext.request.userPrincipal.principal}" />

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation" >
    <ul class="nav navbar-nav">
        <li><a href="<c:url value='/'/>">Home</a></li>
        <c:if test="${not empty principal}">
            <li><a href="<c:url value='/profile'/>">My Profile</a></li>
        </c:if>
            <li><a href="<c:url value='/publisher/list'/>">Publishers</a></li>
        <li><a href="<c:url value='/shop/list'/>">Stores</a></li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Books</a>
            <ul class="dropdown-menu">
                <li><a href="<c:url value="/shop/books"/>">shop's books</a></li>
                <li><a href="<c:url value="/publisher/books"/>">publisher's books</a></li>
            </ul>
        </li>
        <c:if test="${not empty principal}">
            <li class="pull-right"><a href="<c:url value='/cart/'/>" >Cart (<span id="cart">${fn:length(principal.account.cart.books)}</span>)</a></li>
        </c:if>
    </ul>
</div>