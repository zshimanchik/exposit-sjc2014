package by.sjc.giz.dao.repositiory;

import by.sjc.giz.dao.entity.AuthorEntity;
import by.sjc.giz.dao.entity.BookEntity;
import by.sjc.giz.dao.entity.GenreEntity;
import by.sjc.giz.dao.entity.PostEntity;

import java.util.List;

/**
 * Created by Z on 16.08.14.
 */
public interface BookDao {
    public AuthorEntity getAuthorEntity(String name);
    public GenreEntity getGenreEntity(String name);

    public List<GenreEntity> getAllGenreEntities();
    public List<AuthorEntity> getAllAuthorEntities();

    public BookEntity getBookById(int bookId);

    public void addGenre(GenreEntity genreEntity);
    public void addAuthor(AuthorEntity authorEntity);

    public List<PostEntity> getBookPosts(int bookId);
    
    public void addPost(PostEntity postEntity);
}
