package by.sjc.giz.dao.base;

import org.hibernate.criterion.Criterion;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Z on 07.08.14.
 */
public interface GenericDao<T, PK extends Serializable> {

    PK save(T obj);

    void update(T obj);

    List<T> findAll();

    List<T> findByCriteria(Criterion... criterion);

    T findById(PK id);

    void delete(PK id);

    void delete(T persistantObject);
}
