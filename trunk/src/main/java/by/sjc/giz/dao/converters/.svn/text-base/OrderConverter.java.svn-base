package by.sjc.giz.dao.converters;


import by.sjc.giz.dao.entity.BookEntity;
import by.sjc.giz.dao.entity.OrderEntity;
import by.sjc.giz.dao.entity.OrderRecordEntity;
import by.sjc.giz.dao.entity.PaymentEntity;
import by.sjc.giz.dao.entity.SubjectEntity;
import by.sjc.giz.model.Book;
import by.sjc.giz.model.Order;
import by.sjc.giz.model.OrderRecord;
import by.sjc.giz.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Z on 14.08.14.
 */
@Component
public class OrderConverter {

    @Autowired
    private BookConverter bookConverter;
    @Autowired
    private SubjectConverter subjectConverter;
    @Autowired
    private PaymentConverter paymentConverter;

    public Order convertToOrder(OrderEntity orderEntity) {
        Order order = new Order();

        order.setId(orderEntity.getId());
        order.setAddress(orderEntity.getAddress());
        order.setChangeDate(orderEntity.getChangeDate());
        order.setState(orderEntity.getState());

        Subject sender = subjectConverter.convertToSubject(orderEntity.getSender());
        order.setSender(sender);

        Subject recipient = subjectConverter.convertToSubject(orderEntity.getRecipient());
        order.setRecipient(recipient);

        List<OrderRecord> orderRecords = convertToOrderRecord(orderEntity.getRecords());
        order.setRecords(orderRecords);

        PaymentEntity paymentEntity = orderEntity.getPayment();
        order.setPayment(paymentConverter.convertToPayment(paymentEntity));

        return order;
    }
    public OrderEntity convertToOrderEntity(Order order) {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setId(order.getId());
        orderEntity.setAddress(order.getAddress());
        orderEntity.setChangeDate(order.getChangeDate());
        orderEntity.setState(order.getState());

        SubjectEntity sender = subjectConverter.convertToSubjectEntity(order.getSender());
        orderEntity.setSender(sender);

        SubjectEntity recipient = subjectConverter.convertToSubjectEntity(order.getRecipient());
        orderEntity.setRecipient(recipient);

        List<OrderRecordEntity> orderRecordEntities = convertToOrderRecordEntity(order.getRecords());
        orderEntity.setRecords(orderRecordEntities);

        PaymentEntity paymentEntity = paymentConverter.convertToPaymentEntity(order.getPayment());
        orderEntity.setPayment(paymentEntity);

        return orderEntity;
    }

    public List<Order> convertToOrder(List<OrderEntity> orderEntities) {
        List<Order> orders = new ArrayList<Order>(orderEntities.size());
        for (OrderEntity orderEntity : orderEntities)
            orders.add(convertToOrder(orderEntity));
        return orders;
    }
    public List<OrderEntity> convertToOrderEntities(List<Order> orders) {
        List<OrderEntity> orderEntities = new ArrayList<OrderEntity>( orders.size() );
        for (Order order : orders)
            orderEntities.add( convertToOrderEntity(order) );
        return orderEntities;
    }

    public OrderRecord convertToOrderRecord(OrderRecordEntity orderRecordEntity) {
        Book book = bookConverter.convertToBook(orderRecordEntity.getBook());
        return new OrderRecord(book, orderRecordEntity.getCount(), orderRecordEntity.getBookPrice());
    }
    public OrderRecordEntity convertToOrderRecordEntity(OrderRecord orderRecord) {
        BookEntity bookEntity = bookConverter.convertToBookEntity(orderRecord.getBook());
        return new OrderRecordEntity(bookEntity, orderRecord.getCount(), orderRecord.getBookPrice());
    }

    public List<OrderRecord> convertToOrderRecord(List<OrderRecordEntity> orderRecordEntities) {
        List<OrderRecord> orderRecords = new ArrayList<OrderRecord>( orderRecordEntities.size() );
        for (OrderRecordEntity orderRecordEntity : orderRecordEntities)
            orderRecords.add( convertToOrderRecord(orderRecordEntity) );
        return orderRecords;
    }
    public List<OrderRecordEntity> convertToOrderRecordEntity(List<OrderRecord> orderRecords) {
        List<OrderRecordEntity> orderRecordEntities = new ArrayList<OrderRecordEntity>( orderRecords.size() );
        for (OrderRecord orderRecord : orderRecords)
            orderRecordEntities.add( convertToOrderRecordEntity( orderRecord ) );
        return orderRecordEntities;
    }
    
}
