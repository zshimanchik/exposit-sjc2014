package by.sjc.giz.dao.repositiory;

import by.sjc.giz.dao.entity.BookEntity;
import by.sjc.giz.dao.entity.SubjectEntity;
import by.sjc.giz.dao.entity.publisher.ContractWithAuthorsEntity;
import by.sjc.giz.dao.entity.publisher.DiscountEntity;
import by.sjc.giz.model.BooksFilter;

import java.util.List;

/**
 * Created by Z on 11.08.14.
 */
public interface PublisherDao {
    public List<SubjectEntity> getPublisherList();
    public List<BookEntity> getPublisherBookList(int publisherId);
    public List<ContractWithAuthorsEntity> getContractWithAuthorsList(int publisherId);

    public void addContractWithAuthors(ContractWithAuthorsEntity contractEntity);

    public float getBookPrice(int bookId);
    public DiscountEntity getDiscount(int publisherId, int clientId);
    public List<DiscountEntity> getDiscountsList(int publisherId, int pageSize, int pageNumber);
    public void saveOrUpdateDiscountEntity(DiscountEntity discountEntity);

    public List<ContractWithAuthorsEntity> getContracts(int pageSize, int pageNumber, BooksFilter filter);
    public int getContractsCount(BooksFilter filter);


}
