package by.sjc.giz.dao.converters;

import by.sjc.giz.dao.entity.PaymentEntity;
import by.sjc.giz.model.payment.Payment;
import org.springframework.stereotype.Component;

/**
 * Created by Z on 15.08.14.
 */
@Component
public class PaymentConverter {

    public Payment convertToPayment(PaymentEntity paymentEntity) {
        return new Payment(paymentEntity.getId(), paymentEntity.getUserIdInService(), paymentEntity.getMethod());
    }

    public PaymentEntity convertToPaymentEntity(Payment payment) {
        return new PaymentEntity(payment.getId(), payment.getUserIdInService(), payment.getPaymentMethod());
    }
}
