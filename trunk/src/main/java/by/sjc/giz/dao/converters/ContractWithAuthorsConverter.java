package by.sjc.giz.dao.converters;

import by.sjc.giz.dao.entity.BookEntity;
import by.sjc.giz.dao.entity.publisher.ContractWithAuthorsEntity;
import by.sjc.giz.model.Book;
import by.sjc.giz.model.publisher.ContractWithAuthors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Z on 12.08.14.
 */
@Component
public class ContractWithAuthorsConverter {

    @Autowired
    private BookConverter bookConverter;

    public ContractWithAuthors convertToContractWithAuthors(ContractWithAuthorsEntity contractWithAuthorsEntity) {
        Book book = bookConverter.convertToBook(contractWithAuthorsEntity.getBook());

        ContractWithAuthors result = new ContractWithAuthors(
                book,
                contractWithAuthorsEntity.isFixedTax(),
                contractWithAuthorsEntity.getTaxValue(),
                contractWithAuthorsEntity.getSellPrice()
        );
        return result;
    }
    public List<ContractWithAuthors> convertToContractWithAuthors(List<ContractWithAuthorsEntity> contractEntityList ) {
        List<ContractWithAuthors> contractList = new ArrayList<ContractWithAuthors>(
                contractEntityList.size()
        );

        for (ContractWithAuthorsEntity contractEntity : contractEntityList) {
            ContractWithAuthors contract = convertToContractWithAuthors(contractEntity);
            contractList.add(contract);
        }

        return contractList;
    }

    public ContractWithAuthorsEntity convertToContractWithAuthorsEntity(ContractWithAuthors contract) {
        BookEntity bookEntity = bookConverter.convertToBookEntity(contract.getBook());
        ContractWithAuthorsEntity contractEntity = new ContractWithAuthorsEntity(
                bookEntity,
                contract.getIsFixedTax(),
                contract.getSellPrice(),
                contract.getTaxValue()
        );
        return contractEntity;
    }

}
