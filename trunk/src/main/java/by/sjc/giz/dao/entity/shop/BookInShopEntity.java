package by.sjc.giz.dao.entity.shop;

import by.sjc.giz.dao.entity.BookEntity;
import by.sjc.giz.dao.entity.SubjectEntity;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Created by irina on 12.08.14.
 */

@Entity
@Table(name = "shop_store",
        uniqueConstraints = { @UniqueConstraint(columnNames = {"book_id", "shop_id"})} )
public class BookInShopEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id", nullable = false)
    private BookEntity bookEntity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "shop_id", nullable = false)
    private SubjectEntity shop;

    @Column(name = "count")
    private int count;

    @Column(name = "book_sell_price")
    private float sellPrice;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BookEntity getBookEntity() {
        return bookEntity;
    }

    public void setBookEntity(BookEntity bookEntity) {
        this.bookEntity = bookEntity;
    }

    public SubjectEntity getShop() {
        return shop;
    }

    public void setShop(SubjectEntity shop) {
        this.shop = shop;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public float getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(float sellPrice) {
        this.sellPrice = sellPrice;
    }

    @Override
    public String toString() {
        return "BookInShopEntity{" +
                "id=" + id +
                ", bookEntity=" + bookEntity +
                ", shop=" + shop +
                ", count=" + count +
                ", sellPrice=" + sellPrice +
                '}';
    }
}
