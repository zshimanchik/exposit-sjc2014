package by.sjc.giz.dao.repositiory;

import by.sjc.giz.dao.entity.SubjectEntity;
import by.sjc.giz.dao.entity.shop.BookInShopEntity;
import by.sjc.giz.model.BooksFilter;

import java.util.Date;
import java.util.List;

/**
 * Created by irina on 10.08.14.
 */
public interface ShopDao {

    public List<SubjectEntity> getShopList();
    public List<BookInShopEntity> getBookList(int shopId);
    public int getBookCount(int shopId, int bookId);
    public BookInShopEntity getBookInShopById(int shopId, int bookId);
    public void updateBookInShop(BookInShopEntity book);
    public List<String> getProfitableGenres(int shopId);
    public void addBookToStore(BookInShopEntity book);

    public float getBookPrice(int shopId, int bookId);
    public List<BookInShopEntity> getBooks(int pageSize, int pageNumber, BooksFilter filter);
    public int getBooksCount(BooksFilter filter);

    public float getProfit(int shopId, Date startDate, Date endDate);

    public Date getDateOfFirstOrder(int shopId);
    public Date getDateOfLastOrder(int shopId);

}
