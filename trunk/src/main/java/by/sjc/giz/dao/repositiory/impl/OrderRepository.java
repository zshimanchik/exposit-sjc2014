package by.sjc.giz.dao.repositiory.impl;

import by.sjc.giz.dao.entity.OrderEntity;
import by.sjc.giz.dao.repositiory.OrderDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Z on 13.08.14.
 */
@Repository
@Transactional
public class OrderRepository /*extends AbstractHibernateDao<OrderEntity,Long>*/ implements OrderDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void saveOrUpdate(OrderEntity order) {
        getSession().saveOrUpdate(order);
    }

    @Override
    public Integer save(OrderEntity order) {
        return (Integer) getSession().save(order);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OrderEntity> getAllOrders() {
        @SuppressWarnings("JpaQlInspection")
        String hql = "from OrderEntity";
        return getSession().createQuery(hql).list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OrderEntity> getOutgoingOrders(int senderId) {
        @SuppressWarnings("JpaQlInspection")
        String hql = "from OrderEntity where sender_id=:senderId and state not in ('SATISFIED','ABORTIVE') order by state";

        return getSession().createQuery(hql)
                .setParameter("senderId",senderId)
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OrderEntity> getIncomingOrders(int recipientId) {
        @SuppressWarnings("JpaQlInspection")
        String hql = "from OrderEntity where recipient_id=:recipientId and state not in ('SATISFIED','ABORTIVE') order by state";

        return getSession().createQuery(hql).
                setParameter("recipientId", recipientId).
                list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OrderEntity> getCompletedOutgoingOrders(int senderId) {
        @SuppressWarnings("JpaQlInspection")
        String hql = "from OrderEntity where sender_id=:senderId and state in ('SATISFIED','ABORTIVE') order by state";

        return getSession().createQuery(hql)
                .setParameter("senderId",senderId)
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OrderEntity> getCompletedIncomingOrders(int recipientId) {
        @SuppressWarnings("JpaQlInspection")
        String hql = "from OrderEntity where recipient_id=:recipientId and state in ('SATISFIED','ABORTIVE') order by state";

        return getSession().createQuery(hql).
                setParameter("recipientId", recipientId).
                list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OrderEntity> getSatisfiedOutgoingOrders(int senderId) {
        @SuppressWarnings("JpaQlInspection")
        String hql = "from OrderEntity where sender_id=:senderId and state = 'SATISFIED' order by state";

        return getSession().createQuery(hql)
                .setParameter("senderId",senderId)
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<OrderEntity> getSatisfiedIncomingOrders(int recipientId) {
        @SuppressWarnings("JpaQlInspection")
        String hql = "from OrderEntity where recipient_id=:recipientId and state = 'SATISFIED' order by state";

        return getSession().createQuery(hql).
                setParameter("recipientId", recipientId).
                list();
    }

    @Override
    public OrderEntity getOrderById(int id) {
        @SuppressWarnings("JpaQlInspection")
        String hql = "from OrderEntity where id=:id";

        return (OrderEntity) getSession().createQuery(hql).
                setParameter("id", id).
                uniqueResult();
    }

}
