package by.sjc.giz.dao.repositiory.impl;

import by.sjc.giz.dao.base.AbstractHibernateDao;
import by.sjc.giz.dao.entity.SubjectEntity;
import by.sjc.giz.dao.repositiory.SubjectDao;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Z on 11.08.14.
 */
@Repository
@Transactional
public class SubjectRepository  extends AbstractHibernateDao<SubjectEntity, Long> implements SubjectDao {
    @Override
    public SubjectEntity getSubjectById(int subjectId) {
        @SuppressWarnings("JpaQlInspection")
        String hql = "from SubjectEntity where id=:subjectId";
        return (SubjectEntity) getSession().createQuery(hql)
                .setParameter("subjectId", subjectId)
                .uniqueResult();
    }
}
