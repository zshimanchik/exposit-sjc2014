package by.sjc.giz.dao.converters;

import by.sjc.giz.dao.entity.publisher.DiscountEntity;
import by.sjc.giz.model.Subject;
import by.sjc.giz.model.publisher.Discount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Z on 01.09.14.
 */
@Component
public class DiscountConverter {
    @Autowired
    private SubjectConverter subjectConverter;

    public Discount convertToDiscount(DiscountEntity discountEntity) {
        Subject publisher = subjectConverter.convertToSubject(discountEntity.getPublisher());
        Subject client = subjectConverter.convertToSubject(discountEntity.getClient());
        Discount discount = new Discount(discountEntity.getId(), publisher, client, discountEntity.getValue());
        discount.setSpendMoney(discountEntity.getSpendMoney());
        return discount;
    }

    public List<Discount> convertToDiscount(List<DiscountEntity> discountEntities) {
        List<Discount> discounts = new ArrayList<Discount>( discountEntities.size() );
        for (DiscountEntity discountEntity : discountEntities)
            discounts.add( convertToDiscount(discountEntity) );
        return discounts;
    }

    public DiscountEntity convertToDiscountEntity(Discount discount) {
        DiscountEntity discountEntity = new DiscountEntity();
        discountEntity.setId(discount.getId());
        discountEntity.setSpendMoney(discount.getSpendMoney());
        discountEntity.setValue(discount.getValue());
        discountEntity.setClient(subjectConverter.convertToSubjectEntity(discount.getClient()));
        discountEntity.setPublisher(subjectConverter.convertToSubjectEntity(discount.getPublisher()));

        return discountEntity;
    }
}
