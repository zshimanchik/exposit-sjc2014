package by.sjc.giz.dao.entity;

import by.sjc.giz.model.OrderStatus;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

/**
 * Created by Z on 13.08.14.
 */
@Entity
@Table(name = "orders")
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sender_id")
    private SubjectEntity sender;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "recipient_id")
    private SubjectEntity recipient;

    @Embedded
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "order_books", joinColumns = @JoinColumn(name="order_id"))
    private List<OrderRecordEntity> records;

    @Column(name = "address")
    private String address;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "payment_id")
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    private PaymentEntity payment;

    @Temporal(TemporalType.DATE)
    @Column(name = "change_date")
    private Date changeDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "state")
    private OrderStatus state;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public SubjectEntity getSender() {
        return sender;
    }
    public void setSender(SubjectEntity sender) {
        this.sender = sender;
    }

    public SubjectEntity getRecipient() {
        return recipient;
    }
    public void setRecipient(SubjectEntity recipient) {
        this.recipient = recipient;
    }

    public List<OrderRecordEntity> getRecords() {
        return records;
    }
    public void setRecords(List<OrderRecordEntity> records) {
        this.records = records;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public PaymentEntity getPayment() {
        return payment;
    }
    public void setPayment(PaymentEntity payment) {
        this.payment = payment;
    }

    public Date getChangeDate() {
        return changeDate;
    }
    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

    public OrderStatus getState() {
        return state;
    }
    public void setState(OrderStatus state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "OrderEntity{" +
                "id=" + id +
                ", sender=" + sender +
                ", recipient=" + recipient +
                ", records=" + records +
                ", address='" + address + '\'' +
                ", payment=" + payment +
                ", changeDate=" + changeDate +
                ", state=" + state +
                '}';
    }
}
