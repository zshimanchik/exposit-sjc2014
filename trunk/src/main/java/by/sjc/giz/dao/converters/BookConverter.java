package by.sjc.giz.dao.converters;

import java.util.ArrayList;
import java.util.List;

import by.sjc.giz.model.Subject;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.sjc.giz.dao.entity.AccountEntity;
import by.sjc.giz.dao.entity.AuthorEntity;
import by.sjc.giz.dao.entity.BookEntity;
import by.sjc.giz.dao.entity.GenreEntity;
import by.sjc.giz.dao.entity.PostEntity;
import by.sjc.giz.dao.entity.SubjectEntity;
import by.sjc.giz.dao.entity.shop.BookInShopEntity;
import by.sjc.giz.dao.repositiory.BookDao;
import by.sjc.giz.model.Book;
import by.sjc.giz.model.Post;
import by.sjc.giz.model.publisher.Publisher;
import by.sjc.giz.model.shop.BookInShop;

/**
 * Created by Z on 11.08.14.
 */
@Component
public class BookConverter {
    public static final Logger logger = Logger.getLogger(BookConverter.class);

    @Autowired
    private BookDao bookDao;
    @Autowired
    private SubjectConverter subjectConverter;
    @Autowired
    private AccountConverter accountConverter;

    public Book convertToBook(BookEntity bookEntity) {
        Book book = new Book();
        book.setId( bookEntity.getId() );
        book.setName( bookEntity.getName() );
        book.setReleaseDate( bookEntity.getReleaseDate() );

        Publisher publisher = (Publisher) subjectConverter.convertToSubject(bookEntity.getPublisher());
        book.setPublisher(publisher);

        for (GenreEntity genre : bookEntity.getGenreEntitySet()) {
            book.addGenre(genre.getName());
        }
        for (AuthorEntity author : bookEntity.getAuthorEntitySet()) {
            book.addAuthor(author.getName());
        }

        return book;
    }

    public List<Book> convertToBook(List<BookEntity> bookEntityList) {
        List<Book> bookList = new ArrayList<Book>( bookEntityList.size() );

        for (BookEntity bookEntity : bookEntityList) {
            Book book = convertToBook(bookEntity);
            bookList.add(book);
        }
        return bookList;
    }

    public BookInShop convertToBookInShop(BookInShopEntity bookInShopEntity) {

        if(bookInShopEntity == null) {
            return null;
        }
        BookInShop bookInShop = new BookInShop();
        BookEntity bookEntity = bookInShopEntity.getBookEntity();

        Book book = convertToBook(bookEntity);
        bookInShop.setBook(book);

        Subject shop = subjectConverter.convertToSubject( bookInShopEntity.getShop() );
        bookInShop.setShop(shop);

        bookInShop.setCount(bookInShopEntity.getCount());
        bookInShop.setSellPrice(bookInShopEntity.getSellPrice());
        bookInShop.setId(bookInShopEntity.getId());

        return bookInShop;
    }

    public List<BookInShop> convertToBookInShop(List<BookInShopEntity> bookInShopEntityList) {
        List<BookInShop> bookInShopList = new ArrayList<BookInShop>( bookInShopEntityList.size() );
        for(BookInShopEntity bookEntity : bookInShopEntityList) {
            bookInShopList.add( convertToBookInShop(bookEntity) );
        }
        return bookInShopList;
    }

    public BookInShopEntity convertToBookInShopEntity(BookInShop bookInShop) {
        BookInShopEntity bookInShopEntity = new BookInShopEntity();
        bookInShopEntity.setId(bookInShop.getId());
        bookInShopEntity.setCount(bookInShop.getCount());
        bookInShopEntity.setSellPrice(bookInShop.getSellPrice());

        BookEntity bookEntity = convertToBookEntity(bookInShop.getBook());
        bookInShopEntity.setBookEntity(bookEntity);

        SubjectEntity shop = subjectConverter.convertToSubjectEntity(bookInShop.getShop());
        bookInShopEntity.setShop(shop);

        return  bookInShopEntity;
    }
    
    public BookEntity convertToBookEntity(Book book) {
        BookEntity bookEntity = new BookEntity();
        bookEntity.setId( book.getId() );
        bookEntity.setName( book.getName() );
        bookEntity.setReleaseDate( book.getReleaseDate() );

        SubjectEntity publisher = subjectConverter.convertToSubjectEntity(book.getPublisher());
        bookEntity.setPublisher(publisher);

        for (String genre : book.getGenreList()) {
            bookEntity.addGenreEntity(getGenreEntity(genre));
        }
        for (String author : book.getAuthorList()) {
            bookEntity.addAuthorEntity(getAuthorEntity(author));
        }

        return bookEntity;
    }

    public AuthorEntity getAuthorEntity(String authorName) {
        AuthorEntity authorEntity = bookDao.getAuthorEntity(authorName);
        if (authorEntity != null) {
            return authorEntity;
        } else {
            authorEntity = new AuthorEntity();
            authorEntity.setName(authorName);
            return  authorEntity;
        }
    }
    public GenreEntity getGenreEntity(String genreName) {
        GenreEntity genreEntity = bookDao.getGenreEntity(genreName);
        if (genreEntity != null) {
            return genreEntity;
        } else {
            genreEntity = new GenreEntity();
            genreEntity.setName(genreName);
            return genreEntity;
        }
    }
    
    public Post convertToPost(PostEntity postEntity)
    {
    	Post post = new Post(convertToBook(postEntity.getBook()), accountConverter.convertToAccount(postEntity.getAccount()), postEntity.getText());
    	return post;
    }
    
    public PostEntity convertToPostEntity(Post post)
    {
    	BookEntity bookEntity = convertToBookEntity(post.getTargetBook());
    	AccountEntity accountEntity = accountConverter.convertToAccountEntity(post.getPostAuthor());
    	PostEntity postEntity = new PostEntity();
    	postEntity.setBook(bookEntity);
    	postEntity.setAccount(accountEntity);
    	postEntity.setText(post.getPostText());
    	return postEntity;    	
    }
}
