package by.sjc.giz.dao.entity;

import by.sjc.giz.model.payment.PaymentMethod;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * Created by Z on 13.08.14.
 */
@Entity
@Table(name = "payments")
public class PaymentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "user_id_in_service")
    String userIdInService;

    @Enumerated(EnumType.STRING)
    @Column(name = "method")
    PaymentMethod method;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getUserIdInService() {
        return userIdInService;
    }
    public void setUserIdInService(String userIdInService) {
        this.userIdInService = userIdInService;
    }

    public PaymentMethod getMethod() {
        return method;
    }
    public void setMethod(PaymentMethod method) {
        this.method = method;
    }


    public PaymentEntity() {
    }
    public PaymentEntity(int id, String userIdInService, PaymentMethod method) {
        this.id = id;
        this.userIdInService = userIdInService;
        this.method = method;
    }

    @Override
    public String toString() {
        return "PaymentEntity{" +
                "id=" + id +
                ", method=" + method +
                ", userIdInService='" + userIdInService + '\'' +
                '}';
    }
}
