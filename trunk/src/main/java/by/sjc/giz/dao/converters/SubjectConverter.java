package by.sjc.giz.dao.converters;

import by.sjc.giz.dao.entity.SubjectEntity;
import by.sjc.giz.model.Subject;
import by.sjc.giz.model.customer.Customer;
import by.sjc.giz.model.publisher.Publisher;
import by.sjc.giz.model.shop.Shop;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Z on 09.08.14.
 */
@Component
public class SubjectConverter {

    public SubjectEntity convertToSubjectEntity(Subject subject) {
        SubjectEntity subjectEntity = new SubjectEntity();
        subjectEntity.setId( subject.getId() );
        subjectEntity.setName( subject.getName() );
        subjectEntity.setType( subject.getType() );
        return subjectEntity;
    }

    public Subject convertToSubject(SubjectEntity subjectEntity) {
        switch (subjectEntity.getType()) {
            case CUSTOMER: return convertToCustomer(subjectEntity);
            case SHOP: return convertToShop(subjectEntity);
            case PUBLISHER: return convertToPublisher(subjectEntity);
        }
        return null;
    }

    public List<Subject> convertToSubject(List<SubjectEntity> subjectEntityList) {
        List<Subject> subjectList = new ArrayList<Subject>( subjectEntityList.size() );

        for (SubjectEntity subjectEntity : subjectEntityList) {
            Subject subject = convertToSubject(subjectEntity);
            subjectList.add(subject);
        }
        return subjectList;
    }    
    
    private Customer convertToCustomer(SubjectEntity subjectEntity) {
        return new Customer(subjectEntity.getId(), subjectEntity.getName());
    }
    private Shop convertToShop(SubjectEntity subjectEntity) {
        return new Shop(subjectEntity.getId(), subjectEntity.getName());
    }
    private Publisher convertToPublisher(SubjectEntity subjectEntity) {
        return new Publisher(subjectEntity.getId(), subjectEntity.getName());
    }
}
