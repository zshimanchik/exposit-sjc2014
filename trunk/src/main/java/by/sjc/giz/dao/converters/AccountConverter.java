package by.sjc.giz.dao.converters;

import by.sjc.giz.dao.entity.AccountEntity;
import by.sjc.giz.dao.entity.SubjectEntity;
import by.sjc.giz.model.Account;
import by.sjc.giz.model.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Z on 09.08.14.
 */
@Component
public class AccountConverter {

    @Autowired
    private SubjectConverter subjectConverter;

    public Account convertToAccount(AccountEntity accountEntity) {
        Subject subject = subjectConverter.convertToSubject(accountEntity.getSubject());
        Account ac = new Account(
                accountEntity.getId(),
                accountEntity.getEmail(),
                accountEntity.getPassword(),
                subject
        );
        ac.setFirstName(accountEntity.getFirstName());
        ac.setLastName(accountEntity.getLastName());

        return ac;
    }

    public AccountEntity convertToAccountEntity(Account account) {
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setId(account.getId());
        accountEntity.setEmail(account.getEmail());
        accountEntity.setPassword(account.getPassword());
        accountEntity.setFirstName(account.getFirstName());
        accountEntity.setLastName(account.getLastName());
        SubjectEntity subjectEntity = subjectConverter.convertToSubjectEntity(account.getSubject());
        accountEntity.setSubject(subjectEntity);
        return accountEntity;
    }
}
