package by.sjc.giz.dao.repositiory;

import by.sjc.giz.dao.entity.SubjectEntity;

/**
 * Created by Z on 11.08.14.
 */
public interface SubjectDao {
    public SubjectEntity getSubjectById(int subjectId);
}
