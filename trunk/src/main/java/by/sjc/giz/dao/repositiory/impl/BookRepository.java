package by.sjc.giz.dao.repositiory.impl;

import by.sjc.giz.dao.entity.AuthorEntity;
import by.sjc.giz.dao.entity.BookEntity;
import by.sjc.giz.dao.entity.GenreEntity;
import by.sjc.giz.dao.entity.PostEntity;
import by.sjc.giz.dao.entity.SubjectEntity;
import by.sjc.giz.dao.entity.publisher.ContractWithAuthorsEntity;
import by.sjc.giz.dao.entity.shop.BookInShopEntity;
import by.sjc.giz.dao.repositiory.BookDao;
import by.sjc.giz.model.SubjectType;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Z on 16.08.14.
 */
@Repository
@Transactional
public class BookRepository implements BookDao {

    public static final Logger logger = Logger.getLogger(BookRepository.class);

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public GenreEntity getGenreEntity(String name) {
        @SuppressWarnings("JpaQlInspection")
        String hql = "from GenreEntity where name=:name";
        return (GenreEntity) getSession().createQuery(hql)
                                .setParameter("name", name)
                                .uniqueResult();
    }

    @Override
    public AuthorEntity getAuthorEntity(String name) {
        @SuppressWarnings("JpaQlInspection")
        String hql = "from AuthorEntity where name=:name";
        return (AuthorEntity) getSession().createQuery(hql)
                                    .setParameter("name", name)
                                    .uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<GenreEntity> getAllGenreEntities() {
        @SuppressWarnings("JpaQlInspection")
        String hql = "from GenreEntity";
        return getSession().createQuery(hql)
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<AuthorEntity> getAllAuthorEntities() {
        @SuppressWarnings("JpaQlInspection")
        String hql = "from AuthorEntity";
        return getSession().createQuery(hql)
                .list();
    }

	@Override
	public BookEntity getBookById(int bookId) {
        @SuppressWarnings("JpaQlInspection")
		String hql = "from BookEntity where id=:bookId";
        return (BookEntity) getSession().createQuery(hql)
                                    .setParameter("bookId",bookId)
                                    .uniqueResult();
	}


    @Override
    public void addGenre(GenreEntity genreEntity) {
        getSession().save(genreEntity);
    }

    @Override
    public void addAuthor(AuthorEntity authorEntity) {
        getSession().save(authorEntity);
    }

    @SuppressWarnings("unchecked")
    @Override
	public List<PostEntity> getBookPosts(int bookId) {
		@SuppressWarnings("JpaQlInspection")
		String hql = "from PostEntity where book_id=:bookId";
		return getSession().createQuery(hql).setParameter("bookId",bookId).list();
	}

	@Override
	public void addPost(PostEntity postEntity) {
        logger.info("saving =" + postEntity);
        getSession().save(postEntity);
        logger.info(":::::::::::::::::end saving:::::::::::::::");
		
	}
}
