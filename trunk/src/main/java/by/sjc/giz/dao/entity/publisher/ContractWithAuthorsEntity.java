package by.sjc.giz.dao.entity.publisher;

import by.sjc.giz.dao.entity.BookEntity;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToOne;

/**
 * Created by Z on 12.08.14.
 */
@Entity
@Table(name = "publisher_contracts")
public class ContractWithAuthorsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id")
    @Cascade({CascadeType.ALL})
    private BookEntity book;

    @Column(name = "is_fixed_tax")
    private boolean isFixedTax;

    @Column(name = "sell_price")
    private float sellPrice;

    @Column(name = "tax_value")
    private float taxValue;


    public BookEntity getBook() {
        return book;
    }

    public void setBook(BookEntity book) {
        this.book = book;
    }

    public boolean isFixedTax() {
        return isFixedTax;
    }

    public void setFixedTax(boolean isFixedTax) {
        this.isFixedTax = isFixedTax;
    }

    public float getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(float sellPrice) {
        this.sellPrice = sellPrice;
    }

    public float getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(float taxValue) {
        this.taxValue = taxValue;
    }

    @Override
    public String toString() {
        return "ContractWithAuthorsEntity{" +
                "book=" + book +
                ", isFixedTax=" + isFixedTax +
                ", sellPrice=" + sellPrice +
                ", taxValue=" + taxValue +
                '}';
    }

    public ContractWithAuthorsEntity() {
    }
    public ContractWithAuthorsEntity( BookEntity book, boolean isFixedTax, float sellPrice, float taxValue) {
        this.book = book;
        this.isFixedTax = isFixedTax;
        this.sellPrice = sellPrice;
        this.taxValue = taxValue;
    }
}
