package by.sjc.giz.dao.entity;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

/**
 * Created by Z on 21.08.14.
 */
@Embeddable
public class OrderRecordEntity {
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id")
    private BookEntity book;

    @Column(name = "count")
    private int count;

    @Column(name = "book_price")
    private float bookPrice;

    public OrderRecordEntity() {
    }
    public OrderRecordEntity(BookEntity book, int count, float bookPrice) {
        this.book = book;
        this.count = count;
        this.bookPrice = bookPrice;
    }

    public BookEntity getBook() {
        return book;
    }
    public void setBook(BookEntity book) {
        this.book = book;
    }

    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }

    public float getBookPrice() {
        return bookPrice;
    }
    public void setBookPrice(float bookPrice) {
        this.bookPrice = bookPrice;
    }

    @Override
    public String toString() {
        return "OrderRecordEntity{" +
                "bookPrice=" + bookPrice +
                ", count=" + count +
                ", book=" + book +
                '}';
    }
}
