package by.sjc.giz.dao.entity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Z on 11.08.14.
 */

@Entity
@Table(name = "books")
public class BookEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int id;

    @Column(name = "name")
    private String name;


    @Temporal(TemporalType.DATE)
    @Column(name = "release_date")
    private Date releaseDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @Cascade({CascadeType.ALL})
    @JoinTable(
            name = "books_genres",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id")
    )
    private Set<GenreEntity> genreEntitySet;


    @ManyToMany(fetch = FetchType.EAGER)
    @Cascade({CascadeType.ALL})
    @JoinTable(
            name = "books_authors",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id")
    )
    private Set<AuthorEntity> authorEntitySet;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "publisher_id")
    private SubjectEntity publisher;


    public BookEntity() {
        genreEntitySet = new HashSet<GenreEntity>();
        authorEntitySet = new HashSet<AuthorEntity>();
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }
    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Set<GenreEntity> getGenreEntitySet() {
        return genreEntitySet;
    }
    public void setGenreEntitySet(Set<GenreEntity> genreEntitySet) {
        this.genreEntitySet = genreEntitySet;
    }
    public void addGenreEntity(GenreEntity genreEntity) {
        genreEntitySet.add(genreEntity);
    }

    public Set<AuthorEntity> getAuthorEntitySet() {
        return authorEntitySet;
    }
    public void setAuthorEntitySet(Set<AuthorEntity> authorEntitySet) {
        this.authorEntitySet = authorEntitySet;
    }
    public void addAuthorEntity(AuthorEntity authorEntity) {
        authorEntitySet.add(authorEntity);
    }

    public SubjectEntity getPublisher() {
        return publisher;
    }
    public void setPublisher(SubjectEntity publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return "BookEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", releaseDate=" + releaseDate +
                ", genreEntitySet=" + genreEntitySet +
                ", authorEntitySet=" + authorEntitySet +
                ", publisher=" + publisher +
                '}';
    }
}
