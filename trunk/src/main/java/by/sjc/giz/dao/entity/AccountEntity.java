package by.sjc.giz.dao.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * Created by Z on 07.08.14.
 */

@Entity
@Table(name = "accounts",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"id", "email"})}
)
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subject_id", nullable = false)
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    private SubjectEntity subject;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @Cascade({CascadeType.MERGE})
    @JoinTable(
            name = "favorites",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "book_id")
    )
    private Set<BookEntity> favoriteBookEntitiesSet;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SubjectEntity getSubject() {
        return subject;
    }

    public void setSubject(SubjectEntity subject) {
        this.subject = subject;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "AccountEntity{" +
                "id=" + id +
                ", subject=" + subject +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", favorites='" + favoriteBookEntitiesSet + '\'' +
                '}';
    }

	public Set<BookEntity> getFavoriteBookEntitiesSet() {
		return favoriteBookEntitiesSet;
	}

	public void setFavoriteBookEntitiesSet(Set<BookEntity> favoriteBookEntitiesSet) {
		this.favoriteBookEntitiesSet = favoriteBookEntitiesSet;
	}
}
