package by.sjc.giz.dao.repositiory;

import by.sjc.giz.dao.entity.OrderEntity;

import java.util.List;

/**
 * Created by Z on 13.08.14.
 */
public interface OrderDao {

    public void saveOrUpdate(OrderEntity order);
    public Integer save(OrderEntity order);

    public List<OrderEntity> getAllOrders();
    public List<OrderEntity> getOutgoingOrders(int senderId);
    public List<OrderEntity> getIncomingOrders(int recipientId);
    public List<OrderEntity> getCompletedOutgoingOrders(int senderId);
    public List<OrderEntity> getCompletedIncomingOrders(int recipientId);
    public List<OrderEntity> getSatisfiedOutgoingOrders(int senderId);
    public List<OrderEntity> getSatisfiedIncomingOrders(int recipientId);

    public OrderEntity getOrderById(int id);
}
