package by.sjc.giz.dao.repositiory;

import java.util.List;

import by.sjc.giz.dao.base.GenericDao;
import by.sjc.giz.dao.entity.AccountEntity;

/**
 * Created by Z on 07.08.14.
 */
public interface AccountDao extends GenericDao<AccountEntity, Long> {	
    public AccountEntity loadAccountByEmail(String email);
    public AccountEntity loadAccountByCredentials(String email, String passwordHash);
    public AccountEntity loadAccountById(int accId);
    public void registerAccount(AccountEntity accountEntity);
    public List<AccountEntity> getSubjectAccounts(int subjectId);
    public void updateAccount(AccountEntity accountEntity);
}
