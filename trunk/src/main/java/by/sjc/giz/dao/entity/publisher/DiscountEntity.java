package by.sjc.giz.dao.entity.publisher;

import by.sjc.giz.dao.entity.SubjectEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import javax.persistence.Transient;

import java.io.Serializable;

/**
 * Created by Z on 31.08.14.
 */
@Entity
@Table(name = "publisher_discounts")
public class DiscountEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "publisher_id")
    private SubjectEntity publisher;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "client_id")
    private SubjectEntity client;

    @Column(name = "value")
    private int value;

    @Transient
    private float spendMoney;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public SubjectEntity getPublisher() {
        return publisher;
    }
    public void setPublisher(SubjectEntity publisher) {
        this.publisher = publisher;
    }

    public SubjectEntity getClient() {
        return client;
    }
    public void setClient(SubjectEntity client) {
        this.client = client;
    }

    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }

    public float getSpendMoney() {
        return spendMoney;
    }
    public void setSpendMoney(float spendMoney) {
        this.spendMoney = spendMoney;
    }

    @Override
    public String toString() {
        return "DiscountEntity{" +
                "id=" + id +
                ", publisher=" + publisher +
                ", client=" + client +
                ", value=" + value +
                ", spendMoney=" + spendMoney +
                '}';
    }
}
