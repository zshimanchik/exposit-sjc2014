package by.sjc.giz.security;

import by.sjc.giz.model.Account;
import by.sjc.giz.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Z on 09.09.14.
 */
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private AccountService accountService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Account account = accountService.getAccountByEmail(s);
        if (account==null)
            throw (new UsernameNotFoundException("user "+s+" not found"));

        List authorities = new ArrayList();
        authorities.add( new GrantedAuthorityImpl("ROLE_"+account.getSubject().getType().toString()) );

        CustomUserDetails customUserDetails = new CustomUserDetails(
                account.getEmail(),
                account.getPassword(),
                authorities
        );
        customUserDetails.setAccount(account);
        return customUserDetails;
    }
}
