package by.sjc.giz.model;

/**
 * Created by irina on 18.08.14.
 */
public class BookInCart {

    private Book book;
    private Subject seller;
    private int count;

    public Subject getSeller() {
        return seller;
    }

    public void setSeller(Subject seller) {
        this.seller = seller;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public String toString() {

        return "BookInCart{" +
                "book=" + super.toString() +
                "seller=" + seller +
                ", count=" + count +
                '}';
    }
}
