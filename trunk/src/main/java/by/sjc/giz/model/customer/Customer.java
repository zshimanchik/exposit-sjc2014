package by.sjc.giz.model.customer;

import by.sjc.giz.model.Account;
import by.sjc.giz.model.Post;
import by.sjc.giz.model.Subject;
import by.sjc.giz.model.SubjectType;

/**
 * Created by Z on 24.07.14.
 */
public class Customer extends Subject {

    public Customer(){}

    public Customer(String name) {
        super(SubjectType.CUSTOMER, name);
    }

    public Customer(int id, String name) {
        super(id, SubjectType.CUSTOMER, name);
    }
}
