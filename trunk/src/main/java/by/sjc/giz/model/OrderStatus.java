package by.sjc.giz.model;

/**
 * Created by Z on 13.08.14.
 */
public enum OrderStatus {
    NEW,
    WAITING_FOR_BOOK_RELEASES,
    /*
        if recipient can't satisfy client now. e.g. store hasn't enough books.
     */
    WAITING_FOR_RECIPIENT_FEASIBILITY,
    WAITING_FOR_PAY,
    PAYED,
    /*
        if sender change mind. He can cancel it partial while order in state new.
        And he can cancel whole order while it in states NEW, WAITING_FOR_BOOK_RELEASES, WAITING_FOR_PAY.
        this state for recipient. When he will see it - order became ABORTIVE
    */
    SENDER_CANCELED,
    /*
        if recipient don't want or can't satisfy part or whole order.
        He can cancel partial or whole order while it in state NEW, WAITING_FOR_BOOK_RELEASES
        this state for sender. When he will see it - order became ABORTIVE
     */
    RECIPIENT_REJECTED,
    /* Success order */
    SATISFIED,
    /* abortive order */
    ABORTIVE
}
