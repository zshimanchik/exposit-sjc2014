package by.sjc.giz.model.payment;

/**
 * Created by Z on 29.07.14.
 */
public interface PaymentProcessor {
    public void paymentRequest(Payment payment, float value);
    public PaymentStatus checkPaymentStatus(Payment payment);
}
