package by.sjc.giz.model.payment;

/**
 * Created by Z on 28.07.14.
 */
public enum PaymentStatus {
    WAITING_FOR_USER, PAYED, DENIED
}
