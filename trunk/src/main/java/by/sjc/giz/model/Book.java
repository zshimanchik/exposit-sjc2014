package by.sjc.giz.model;

import by.sjc.giz.model.publisher.Publisher;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Book {

    private int id;
    private String name;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date releaseDate;
    private List<String> authorList;
    private List<String> genreList;
    private Publisher publisher;

    public Book() {
        init();
    }

    public Book(String name){
        init();
        this.name = name;
    }

    private void init(){
        authorList = new ArrayList<String>();
        genreList = new ArrayList<String>();
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }
    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Publisher getPublisher() {
        return publisher;
    }
    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public List<String> getGenreList() {
        return genreList;
    }
    public void setGenreList(List<String> genreList) {
        this.genreList = genreList;
    }
    public void addGenre(String genre) {
        genreList.add(genre);
    }

    public List<String> getAuthorList() {
        return authorList;
    }
    public void setAuthorList(List<String> authorList) {
        this.authorList = authorList;
    }
    public void addAuthor(String author) {
        authorList.add(author);
    }

    public boolean getIsReleased() {
        return releaseDate != null && 1 > releaseDate.compareTo(new Date());
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", releaseDate=" + releaseDate +
                ", authorList=" + authorList +
                ", genreList=" + genreList +
                ", publisher=" + publisher +
                '}';
    }
}
