package by.sjc.giz.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Date;

public class Cart {
	
	private List<BookInCart> books;

    public Cart() {
        books = new ArrayList<BookInCart>();
    }

    /**
     * This method return released book from cart and remove it.
     */
    public List<BookInCart> getReleasedBooksBySeller(String sellerName) {
        List<BookInCart> booksInCart = new ArrayList<BookInCart>();

        Iterator<BookInCart> it = this.books.iterator();
        while(it.hasNext()) {
            BookInCart book = it.next();
            if (book.getSeller().getName().equals(sellerName) && book.getBook().getReleaseDate().before(Calendar.getInstance().getTime())) {
                booksInCart.add(book);
                it.remove();
            }
        }
        return booksInCart;
    }

    /**
     * Only return books, not remove.
     */
    public List<BookInCart> getUnreleasedBooksBySeller(String sellerName) {
        List<BookInCart> booksInCart = new ArrayList<BookInCart>();

        for(BookInCart book : this.books) {
            if (book.getSeller().getName().equals(sellerName) && book.getBook().getReleaseDate().after(Calendar.getInstance().getTime())) {
                booksInCart.add(book);
            }
        }
        return booksInCart;
    }

    /**
     * This method return unreleased book by date from cart and remove it.
     */
    public List<BookInCart> getUnreleasedBooksByDate(Date releasedDate) {
        List<BookInCart> booksInCart = new ArrayList<BookInCart>();

        Iterator<BookInCart> it = this.books.iterator();
        while(it.hasNext()) {
            BookInCart book = it.next();
            if (book.getBook().getReleaseDate().equals(releasedDate)) {
                booksInCart.add(book);
                it.remove();
            }
        }
        return booksInCart;
    }

    public void addBook(BookInCart bookInCart) {
        BookInCart existingBook = new BookInCart();
        for (BookInCart book : this.books ) {
            if (book.getBook().getId() == bookInCart.getBook().getId()) {
                existingBook = book;
            }
        }
        if (existingBook.getCount() != 0) {
            existingBook.setCount(bookInCart.getCount());
        } else {
            this.books.add(bookInCart);
        }
    }

    public void deleteBook(int bookId) {
       Iterator<BookInCart> it = this.books.iterator();
        while(it.hasNext()) {
            BookInCart book = it.next();
            if (book.getBook().getId() == bookId) {
                it.remove();
                break;
            }
        }
    }

    public List<BookInCart> getBooks() {
        return books;
    }

    public void setBooks(List<BookInCart> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "books=" + books +
                '}';
    }
}
