package by.sjc.giz.model.shop;

import java.util.List;

/**
 * Created by Z on 27.08.14.
 * >_< f*cking java.
 */
public class BooksWithMaxCount {
    private List<BookInShop> books;
    private int maxCount;

    public List<BookInShop> getBooks() {
        return books;
    }

    public void setBooks(List<BookInShop> books) {
        this.books = books;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public int getPageCount(int pageSize) {
        if (pageSize>0)
            return (int) Math.ceil( getMaxCount() / (float) pageSize);
        else
            return 0;
    }
}
