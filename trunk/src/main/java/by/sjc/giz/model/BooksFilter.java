package by.sjc.giz.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Z on 28.08.14.
 */
public class BooksFilter {
    private String name;
    private Float minPrice;
    private Float maxPrice;
    private List<String> genres;
    private List<String> authors;
    private List<String> shops;
    private List<String> publishers;

    public BooksFilter() {}


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Float getMinPrice() {
        return minPrice;
    }
    public void setMinPrice(Float minPrice) {
        this.minPrice = minPrice;
    }

    public Float getMaxPrice() {
        return maxPrice;
    }
    public void setMaxPrice(Float maxPrice) {
        this.maxPrice = maxPrice;
    }

    public List<String> getGenres() {
        return genres;
    }
    public void setGenres(List<String> genres) {
        this.genres = filterEmpty(genres);
    }
    public void addGenre(String genre) {
        if (genre.equals(""))
            return;
        if (genres==null)
            genres = new ArrayList<String>(1);
        genres.add(genre);
    }

    public List<String> getAuthors() {
        return authors;
    }
    public void setAuthors(List<String> authors) {
        this.authors = filterEmpty(authors);
    }
    public void addAuthor(String author) {
        if (author.equals(""))
            return;
        if (authors==null)
            authors = new ArrayList<String>(1);
        authors.add(author);
    }

    public List<String> getShops() {
        return shops;
    }
    public void setShops(List<String> shops) {
        this.shops = filterEmpty(shops);
    }
    public void setShop(String shop) {
        this.shops = new ArrayList<String>(1);
        this.shops.add(shop);
    }
    public void addShop(String shop) {
        if (shop.equals(""))
            return;
        if (shops==null)
            shops = new ArrayList<String>(1);
        shops.add(shop);
    }

    public List<String> getPublishers() {
        return publishers;
    }
    public void setPublishers(List<String> publishers) {
        this.publishers = filterEmpty(publishers);
    }
    public void setPublisher(String publisher) {
        this.publishers = new ArrayList<String>(1);
        this.publishers.add(publisher);
    }
    public void addPublisher(String publisher) {
        if (publisher.equals(""))
            return;
        if (publishers==null)
            publishers = new ArrayList<String>(1);
        publishers.add(publisher);
    }

    private List<String> filterEmpty(List<String> list) {
        if (list==null)
            return null;

        while (list.remove(""));

        if (list.isEmpty())
            return null;
        else
            return list;
    }

    @Override
    public String toString() {
        return "BooksFilter{" +
                "name='" + name + '\'' +
                ", minPrice=" + minPrice +
                ", maxPrice=" + maxPrice +
                ", genres=" + genres +
                ", authors=" + authors +
                ", shops=" + shops +
                ", publishers=" + publishers +
                '}';
    }
}
