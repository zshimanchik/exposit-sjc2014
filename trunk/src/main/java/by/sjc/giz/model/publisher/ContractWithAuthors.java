package by.sjc.giz.model.publisher;

import by.sjc.giz.model.Book;

import java.util.List;

/**
 * Created by Z on 26.07.14.
 */
public class ContractWithAuthors {
    private Book book;
    private boolean isFixedTax;
    private float taxValue;
    private float sellPrice;

    public ContractWithAuthors() {
        book = new Book();
    }

    public ContractWithAuthors( Book book, boolean isFixedTax, float taxValue, float sellPrice) {
        this.book = book;
        this.isFixedTax = isFixedTax;
        this.taxValue = taxValue;
        this.sellPrice = sellPrice;
    }

    public List<String> getAuthorList() {
        return book.getAuthorList();
    }

    public Book getBook() {
        return book;
    }

    public boolean isFixedTax() {
        return isFixedTax;
    }

    public boolean isPercentTax() {
        return !isFixedTax;
    }

    public float getTaxValue() {
        return taxValue;
    }

    public void setFixedTax(boolean isFixedDuty) {
        this.isFixedTax = isFixedDuty;
    }

    public float getSellPrice() {
        return sellPrice;
    }

    public boolean getIsFixedTax() {
        return isFixedTax;
    }


    @Override
    public String toString() {
        return "ContractWithAuthors{" +
                "book=" + book +
                ", isFixedTax=" + isFixedTax +
                ", taxValue=" + taxValue +
                ", sellPrice=" + sellPrice +
                '}';
    }

    //for bind in form
    public void setBook(Book book) {
        this.book = book;
    }
    public void setTaxValue(float taxValue) {
        this.taxValue = taxValue;
    }
    public void setSellPrice(float sellPrice) {
        this.sellPrice = sellPrice;
    }
}
