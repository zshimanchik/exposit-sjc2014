package by.sjc.giz.model.publisher;

import by.sjc.giz.model.shop.BookInShop;

import java.util.List;

/**
 * Created by Z on 27.08.14.
 * >_< f*cking java.
 */
public class BooksWithMaxCount {
    private List<ContractWithAuthors> contracts;
    private int maxCount;

    public List<ContractWithAuthors> getContracts() {
        return contracts;
    }
    public void setContracts(List<ContractWithAuthors> contracts) {
        this.contracts = contracts;
    }

    public int getMaxCount() {
        return maxCount;
    }
    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public int getPageCount(int pageSize) {
        if (pageSize>0)
            return (int) Math.ceil( getMaxCount() / (float) pageSize);
        else
            return 0;
    }
}
