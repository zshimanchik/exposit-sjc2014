package by.sjc.giz.model.payment;

import javax.validation.constraints.Size;

/**
 * Created by Z on 29.07.14.
 */
public class Payment {

    //when customer are making order, offer him his payment history.
    private int id;

    @Size(min = 1, max = 40, message = "Please, enter your id.")
    private String userIdInService;
    private PaymentMethod paymentMethod;

    public Payment() {
    }

    public Payment(String userIdInService, PaymentMethod paymentMethod) {
        this.userIdInService = userIdInService;
        this.paymentMethod = paymentMethod;
    }

    public Payment(int id, String userIdInService, PaymentMethod paymentMethod) {
        this.id = id;
        this.userIdInService = userIdInService;
        this.paymentMethod = paymentMethod;
    }

    public int getId() {
        return id;
    }

    public String getUserIdInService() {
        return userIdInService;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setUserIdInService(String userIdInService) {
        this.userIdInService = userIdInService;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", userIdInService='" + userIdInService + '\'' +
                ", paymentMethod=" + paymentMethod +
                '}';
    }
}
