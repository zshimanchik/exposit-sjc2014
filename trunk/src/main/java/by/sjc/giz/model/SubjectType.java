package by.sjc.giz.model;

/**
 * Created by Z on 07.08.14.
 */
public enum SubjectType {
    CUSTOMER,SHOP,PUBLISHER;

    public String getValue() {
        return this.name();
    }
}
