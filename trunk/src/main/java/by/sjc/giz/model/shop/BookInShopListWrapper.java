package by.sjc.giz.model.shop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by irina on 30.08.14.
 */
public class BookInShopListWrapper {

    private List<BookInShop> bookInShopList;

    public BookInShopListWrapper() {
        bookInShopList = new ArrayList<BookInShop>();
    }

    public List<BookInShop> getBookInShopList() {
        return bookInShopList;
    }

    public void setBookInShopList(List<BookInShop> bookInShopList) {
        this.bookInShopList = bookInShopList;
    }

    public void add(BookInShop bookInShop) {
        this.bookInShopList.add(bookInShop);
    }
}
