package by.sjc.giz.model.shop;

import by.sjc.giz.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Shop extends Subject {

    public Shop(String name){
        super(SubjectType.SHOP, name);
    }

    public Shop(int id, String name) {
        super(id, SubjectType.SHOP, name);
    }
}
