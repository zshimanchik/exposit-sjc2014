package by.sjc.giz.model.payment;

/**
 * Created by Z on 29.07.14.
 */
public enum PaymentMethod {
    CASH, WEBMONEY, SMS;

    public String getValue() {
        return this.name();
    }
}
