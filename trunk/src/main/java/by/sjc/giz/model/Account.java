package by.sjc.giz.model;

import by.sjc.giz.model.customer.Customer;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Account {

    private int id;
    private Subject subject;
    private Cart cart;

    @NotNull (message = "Your email is incorrect")
    private String email;

    @NotNull (message = "Please enter your password")
    @Size(min = 6, max = 20, message = "Password size must be between 6 and 20")
    private String password;

    @Pattern(regexp="[a-zA-Z]*", message = "Enter correct first name")
    private String firstName;

    @Pattern(regexp="[a-zA-Z]*", message = "Enter correct last name")
    private String lastName;

    public Account() {
        this.cart = new Cart();
    }

    public Account(String email, String password){
        this.email = email;
        this.password = password;
        this.subject = new Customer();
        this.cart = new Cart();
    }

    public Account(int id, String email, String password, Subject subject){
        this.id = id;
        this.email = email;
        this.password = password;
        this.subject = subject;
        this.cart = new Cart();
    }

    public int getId() {
        return id;
    }

    public Subject getSubject() {
        return subject;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getPassword() {
        return password;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", subject=" + subject +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }



    public void setPassword(String password) {
        this.password = password;
    }

	public Cart getCart() {
		return cart;
	}

	public void setCart(Cart cart) {
		this.cart = cart;
	}

}
