package by.sjc.giz.model;

public class Post {
	
	private Book targetBook;
	private Account postAuthor;
	private String postText;
	
	public Post(Book targetBook, Account postAuthor, String postText) {
		super();
		this.targetBook = targetBook;
		this.postAuthor = postAuthor;
		this.postText = postText;
	}
	
	public Post()
	{
		super();
	}
	
	public String getPostText() {
		return postText;
	}
	
	public void setPostText(String postText) {
		this.postText = postText;
	}
	
	public Book getTargetBook() {
		return targetBook;
	}
	
	public Account getPostAuthor() {
		return postAuthor;
	}

    @Override
    public String toString() {
        return "Post{" +
                "targetBook=" + targetBook +
                ", postAuthor=" + postAuthor +
                ", postText='" + postText + '\'' +
                '}';
    }
}
