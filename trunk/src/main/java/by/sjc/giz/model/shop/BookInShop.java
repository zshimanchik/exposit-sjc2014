package by.sjc.giz.model.shop;

import by.sjc.giz.model.Book;
import by.sjc.giz.model.Subject;

/**
 * Created by irina on 30.07.14.
 */
public class BookInShop {

    private int id;
    private Book book;
    private Subject shop;
    private int count;
    private float sellPrice;

    public BookInShop()
    {   book = new Book(); }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public float getSellPrice() {
        return sellPrice;
    }
    public void setSellPrice(float sellPrice) {
        this.sellPrice = sellPrice;
    }

    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }

	public Subject getShop() {
		return shop;
	}
	public void setShop(Subject shop) {
		this.shop = shop;
	}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BookInShop{" +
                "count=" + count +
                ", sellPrice=" + sellPrice +
                ", shop=" + shop +
                '}';
    }
}
