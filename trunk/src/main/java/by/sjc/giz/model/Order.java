package by.sjc.giz.model;

import by.sjc.giz.model.payment.Payment;

import java.util.*;

/**
 * Created by irina on 28.07.14.
 */
public class Order {

    private int id;
    private Subject sender;
    private Subject recipient;
    private List<OrderRecord> records;
    private String address;
    private Payment payment;
    private Date changeDate;
    private OrderStatus state;

    public Order() {
        records = new ArrayList<OrderRecord>();
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public Subject getSender() {
        return sender;
    }
    public void setSender(Subject sender) {
        this.sender = sender;
    }

    public Subject getRecipient() {
        return recipient;
    }
    public void setRecipient(Subject recipient) {
        this.recipient = recipient;
    }

    public List<OrderRecord> getRecords() {
        return records;
    }
    public void setRecords(List<OrderRecord> records) {
        this.records = records;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public Payment getPayment() {
        return payment;
    }
    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Date getChangeDate() {
        return changeDate;
    }
    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

    public OrderStatus getState() {
        return state;
    }
    public void setState(OrderStatus state) {
        this.state = state;
    }

    public float getCost() {
        float sum = 0;
        for (OrderRecord record : records)
            sum += record.getBookPrice() * record.getCount();
        return sum;
    }

    public boolean getIsPreorder() {
        Iterator<OrderRecord> iter = records.iterator();
        while (iter.hasNext()) {
            if ( !iter.next().getBook().getIsReleased() )
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", sender=" + sender +
                ", recipient=" + recipient +
                ", records=" + records +
                ", address='" + address + '\'' +
                ", payment=" + payment +
                ", changeDate=" + changeDate +
                ", state=" + state +
                '}';
    }

    public void addBook(Book book, int count, float bookPrice) {
        this.records.add(new OrderRecord(book, count, bookPrice));
    }
}
