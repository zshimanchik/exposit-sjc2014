package by.sjc.giz.model.publisher;

import by.sjc.giz.model.*;

import java.util.ArrayList;
import java.util.List;

public class Publisher extends Subject {

    public Publisher(String name) {
        super(SubjectType.PUBLISHER, name);
    }

    public Publisher(int id, String name) {
        super(id, SubjectType.PUBLISHER, name);
    }
}
