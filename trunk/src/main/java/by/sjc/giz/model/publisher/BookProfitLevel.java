package by.sjc.giz.model.publisher;

import by.sjc.giz.model.Book;

/**
 * Created by Z on 30.07.14.
 */
public class BookProfitLevel {
    private Book book;
    private float profitLevel;

    public BookProfitLevel(Book book, float profitLevel) {
        this.book = book;
        this.profitLevel = profitLevel;
    }

    public Book getBook() {
        return book;
    }

    public float getProfitLevel() {
        return profitLevel;
    }
}
