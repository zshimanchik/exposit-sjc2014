package by.sjc.giz.model.publisher;

import by.sjc.giz.model.Subject;

/**
 * Created by Z on 29.07.14.
 */
public class Discount {
    private int id;
    private Subject publisher;
    private Subject client;
    private int value;
    private float spendMoney;

    public Discount(int id, Subject publisher, Subject client, int value) {
        this.id = id;
        this.publisher = publisher;
        this.client = client;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public Subject getPublisher() {
        return publisher;
    }

    public Subject getClient() {
        return client;
    }

    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }

    public float getSpendMoney() {
        return spendMoney;
    }
    public void setSpendMoney(float spendMoney) {
        this.spendMoney = spendMoney;
    }

    @Override
    public String toString() {
        return "Discount{" +
                "id=" + id +
                ", publisher=" + publisher +
                ", client=" + client +
                ", value=" + value +
                ", spendMoney=" + spendMoney +
                '}';
    }
}
