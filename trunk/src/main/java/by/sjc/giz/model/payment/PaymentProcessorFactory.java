package by.sjc.giz.model.payment;

/**
 * Created by Z on 31.07.14.
 */
public interface PaymentProcessorFactory {
    public PaymentProcessor getPaymentProcessor(Payment payment);
}
