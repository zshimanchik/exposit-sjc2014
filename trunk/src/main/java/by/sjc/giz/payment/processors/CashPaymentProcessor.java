package by.sjc.giz.payment.processors;

import by.sjc.giz.model.payment.PaymentProcessor;
import by.sjc.giz.model.payment.Payment;
import by.sjc.giz.model.payment.PaymentStatus;
import org.apache.log4j.Logger;

/**
 * Created by Z on 28.07.14.
 */
public class CashPaymentProcessor implements PaymentProcessor {

    public static final Logger logger = Logger.getLogger(CashPaymentProcessor.class);

    @Override
    public void paymentRequest(Payment payment, float value) {
        logger.info("requesting CashPayment=" + payment + " value=" + value);
    }

    @Override
    public PaymentStatus checkPaymentStatus(Payment payment) {
        logger.info("checking CashPayment=" + payment);
        return PaymentStatus.PAYED;
    }
}
