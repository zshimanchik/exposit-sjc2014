package by.sjc.giz.payment.processors;

import by.sjc.giz.model.payment.PaymentProcessor;
import by.sjc.giz.model.payment.Payment;
import by.sjc.giz.model.payment.PaymentStatus;
import org.apache.log4j.Logger;

import java.util.Random;

/**
 * Created by Z on 28.07.14.
 */
public class WebmoneyPaymentProcessor implements PaymentProcessor {

    public static final Logger logger = Logger.getLogger(WebmoneyPaymentProcessor.class);

    @Override
    public void paymentRequest(Payment payment, float value) {
        //requesting to webmoney service
        logger.info("requesting WebMoneyPayment="+payment+" value="+value);
    }

    @Override
    public PaymentStatus checkPaymentStatus(Payment payment) {
        //for emulation user interaction with payment service
        logger.info("checking WebMoneyPayment="+payment);
        if (new Random().nextBoolean()) {
            logger.info("payment status=PAYED");
            return PaymentStatus.PAYED;
        } else {
            logger.info("payment status=WAITING_FOR_USER");
            return PaymentStatus.WAITING_FOR_USER;
        }
    }
}
