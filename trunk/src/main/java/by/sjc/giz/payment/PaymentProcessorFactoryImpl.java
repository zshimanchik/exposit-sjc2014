package by.sjc.giz.payment;

import by.sjc.giz.model.payment.PaymentProcessor;
import by.sjc.giz.model.payment.Payment;
import by.sjc.giz.payment.processors.CashPaymentProcessor;
import by.sjc.giz.payment.processors.SmsPaymentProcessor;
import by.sjc.giz.payment.processors.WebmoneyPaymentProcessor;

/**
 * Created by Z on 29.07.14.
 */
public class PaymentProcessorFactoryImpl implements by.sjc.giz.model.payment.PaymentProcessorFactory {
    private static PaymentProcessorFactoryImpl ourInstance = new PaymentProcessorFactoryImpl();

    public static PaymentProcessorFactoryImpl getInstance() {
        return ourInstance;
    }

    private PaymentProcessorFactoryImpl() {
    }

    public PaymentProcessor getPaymentProcessor(Payment payment) {
        switch (payment.getPaymentMethod()) {
            case CASH: return new CashPaymentProcessor();
            case WEBMONEY: return new WebmoneyPaymentProcessor();
            case SMS: return new SmsPaymentProcessor();
        }
        return null;
    }
}
