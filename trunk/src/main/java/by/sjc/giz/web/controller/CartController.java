package by.sjc.giz.web.controller;

import by.sjc.giz.model.Account;
import by.sjc.giz.model.Book;
import by.sjc.giz.model.BookInCart;
import by.sjc.giz.model.Subject;
import by.sjc.giz.security.CustomUserDetails;
import by.sjc.giz.service.BookService;
import by.sjc.giz.service.OrderService;
import by.sjc.giz.service.SubjectService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/cart")
@PreAuthorize("isFullyAuthenticated()")
public class CartController {

    public static final Logger logger = Logger.getLogger(CartController.class);

    @Autowired
    private BookService bookService;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private OrderService orderService;

    @RequestMapping(value = {"/"})
    public ModelAndView cart(Authentication authentication) {
        ModelAndView mav = new ModelAndView();
        Account account = ((CustomUserDetails) authentication.getPrincipal()).getAccount();
        mav.setViewName("cart");
        mav.addObject("price", orderService.getPrice(account.getCart()));
        return mav;
    }

    @RequestMapping(value="/add", method= { RequestMethod.POST })
    @ResponseBody
    public int addBook(
            @RequestBody final BookInCart bookFromPage,
            Authentication authentication) {

        Subject seller = subjectService.getSubjectById(bookFromPage.getSeller().getId());
        Book book = bookService.getBookById(bookFromPage.getBook().getId());
        BookInCart bookInCart = new BookInCart();
        bookInCart.setBook(book);
        bookInCart.setSeller(seller);
        bookInCart.setCount(bookFromPage.getCount());

        Account account = ((CustomUserDetails) authentication.getPrincipal()).getAccount();

        account.getCart().addBook(bookInCart);
        logger.info("ADD BOOK TO CART: " + book.getId() + ", count: " + bookInCart.getCount());

        return account.getCart().getBooks().size();
    }

    @RequestMapping(value="/delete", method= { RequestMethod.POST })
    @ResponseBody
    public Book deleteBook(
            @RequestBody final Book book,
            Authentication authentication) {

        Account account = ((CustomUserDetails) authentication.getPrincipal()).getAccount();
        account.getCart().deleteBook(book.getId());
        logger.info("DELETED BOOK FROM CART: " + book.getId());
        return book;
    }

    @RequestMapping(value="/recalculate", method= { RequestMethod.POST })
    @ResponseBody
    public float recalculate(Authentication authentication) {
        Account account = ((CustomUserDetails) authentication.getPrincipal()).getAccount();
        return orderService.getPrice(account.getCart());
    }

}
