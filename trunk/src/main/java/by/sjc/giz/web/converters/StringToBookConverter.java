package by.sjc.giz.web.converters;

import by.sjc.giz.model.Book;
import by.sjc.giz.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by moon on 05.09.14.
 */
@Component
public class StringToBookConverter implements Converter<String, Book> {

    @Autowired
    private BookService bookService;

    @Override
    public Book convert(String orderId) {
        return bookService.getBookById(new Integer(orderId));
    }
}
