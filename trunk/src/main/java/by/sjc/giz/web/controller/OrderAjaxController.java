package by.sjc.giz.web.controller;

import by.sjc.giz.model.Order;
import by.sjc.giz.service.AccountService;
import by.sjc.giz.service.OrderService;
import by.sjc.giz.service.PublisherService;
import by.sjc.giz.service.SubjectService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Arrays;

/**
 * Created by Z on 18.08.14.
 */
@Controller
@PreAuthorize("isFullyAuthenticated()")
@RequestMapping("/order/ajax")
public class OrderAjaxController {
    public static final Logger logger = Logger.getLogger(OrderAjaxController.class);

    @Autowired
    private OrderService orderService;


    @PreAuthorize("principal.account.subject.id == #order.recipient.id")
    @RequestMapping(value="/rejectBookFromIncomingOrder")
    @ResponseBody
    public Order[] rejectBookFromIncomingOrder(
            @RequestParam("orderId") Order order,
            @RequestParam("bookId") int bookId) {
        logger.info(" rejectBookFromIncomingOrder order="+order+" bookId="+bookId);
        Order[] orders = orderService.rejectBookFromIncomingOrder( order.getId(),bookId );
        logger.info("returned orders:" + Arrays.toString(orders));
        return orders;
    }

    @PreAuthorize("principal.account.subject.id == #order.recipient.id")
    @RequestMapping(value="/rejectIncomingOrder")
    @ResponseBody
    public Order rejectIncomingOrder(
            @RequestParam("orderId") Order order) {
        logger.info(" rejectIncomingOrder order="+order);
        Order retOrder = orderService.rejectIncomingOrder(order.getId());
        logger.info("returned order:"+retOrder);
        return retOrder;
    }

    @PreAuthorize("principal.account.subject.id == #order.recipient.id")
    @RequestMapping(value="/confirmIncomingOrder")
    @ResponseBody
    public Order confirmIncomingOrder(
            @RequestParam("orderId") Order order) {
        logger.info("confirmIncomingOrder order="+order);
        Order retOrder =orderService.confirmIncomingOrder(order.getId());
        logger.info("return order:"+retOrder);
        return retOrder;
    }

    @PreAuthorize("principal.account.subject.id == #order.sender.id")
    @RequestMapping(value="/cancelOutgoingOrder")
    @ResponseBody
    public Order cancelOutgoingOrder(
            @RequestParam("orderId") Order order) {
        logger.info(" cancelOutgoingOrder order="+order);
        Order retOrder = orderService.cancelOutgoingOrder(order.getId());
        logger.info("return order:"+retOrder);
        return retOrder;
    }

    @PreAuthorize("principal.account.subject.id == #order.sender.id")
    @RequestMapping(value="/cancelBookFromOutgoingOrder")
    @ResponseBody
    public Order[] cancelBookFromOutgoingOrder(
            @RequestParam("orderId") Order order,
            @RequestParam("bookId") int bookId) {
        logger.info(" cancelBookFromOutgoingOrder order="+order+" bookId="+bookId);
        Order[] orders = orderService.cancelBookFromOutgoingOrder( order.getId(),bookId );
        logger.info("returned orders:" + Arrays.toString(orders));
        return orders;
    }

    @PreAuthorize("principal.account.subject.id == #order.sender.id")
    @RequestMapping(value="/confirmOutgoingOrder")
    @ResponseBody
    public Order confirmOutgoingOrder(
            @RequestParam("orderId") Order order) {
        logger.info("confirmOutgoingOrder order="+order);
        Order retOrder = orderService.confirmOutgoingOrder(order.getId());
        logger.info("return order:"+retOrder);
        return retOrder;
    }
}
