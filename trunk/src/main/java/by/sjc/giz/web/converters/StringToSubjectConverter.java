package by.sjc.giz.web.converters;

import by.sjc.giz.model.Subject;
import by.sjc.giz.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by Z on 12.08.14.
 */
@Component
public class StringToSubjectConverter implements Converter<String, Subject> {

    @Autowired
    SubjectService subjectService;

    @Override
    public Subject convert(String subjectId) {
        return subjectService.getSubjectById(new Integer(subjectId));
    }
}
