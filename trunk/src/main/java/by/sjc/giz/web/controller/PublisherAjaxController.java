package by.sjc.giz.web.controller;

import by.sjc.giz.model.Subject;
import by.sjc.giz.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * Created by Z on 15.08.14.
 */
@Controller
@RequestMapping("/publisher/ajax")
public class PublisherAjaxController {

    public static final Logger logger = Logger.getLogger(PublisherAjaxController.class);

    @Autowired
    private BookService bookService;
    @Autowired
    private PublisherService publisherService;



    @PreAuthorize("isFullyAuthenticated() and principal.account.subject.type.value == 'PUBLISHER'")
    @RequestMapping(value="/addBook/addGenre")
    @ResponseBody
    public void addGenre(@RequestParam("genreName") String genreName) {
        logger.info("add genre="+genreName);
        bookService.addGenre(genreName);

    }

    @PreAuthorize("isFullyAuthenticated() and principal.account.subject.type.value == 'PUBLISHER'")
    @RequestMapping(value="/addBook/addAuthor")
    @ResponseBody
    public void addAuthor(@RequestParam("authorName") String authorName) {
        logger.info("add author="+authorName);
        bookService.addAuthor(authorName);

    }

    @PreAuthorize("isFullyAuthenticated() and principal.account.subject.id == #publisher.id")
    @RequestMapping(value="/discountChange")
    @ResponseBody
    public void discountChange(
            @RequestParam("publisherId") Subject publisher,
            @RequestParam("clientId") Subject client,
            @RequestParam("value") int value) {

        logger.info("discountChange: publ="+publisher+" client="+client+" value="+value);
        publisherService.changeDiscount(publisher,client,value);
    }


}
