package by.sjc.giz.web.converters;

import by.sjc.giz.model.Order;
import by.sjc.giz.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by irina on 29.08.14.
 */
@Component
public class StringToOrderConverter implements Converter<String, Order> {

    @Autowired
    OrderService orderService;

    @Override
    public Order convert(String orderId) {
        return orderService.getById(new Integer(orderId));
    }
}
