package by.sjc.giz.service;

import by.sjc.giz.model.Subject;

/**
 * Created by Z on 11.08.14.
 */
public interface SubjectService {
    public Subject getSubjectById(int subjectId);
}
