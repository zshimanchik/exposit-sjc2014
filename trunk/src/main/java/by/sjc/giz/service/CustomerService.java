package by.sjc.giz.service;

import java.util.List;

import by.sjc.giz.dao.entity.BookEntity;
import by.sjc.giz.model.Account;
import by.sjc.giz.model.Book;

/**
 * Created by irina on 30.07.14.
 */
public interface CustomerService {

    public void addToFavorites(Account customer, Book book);
    public void removeFromFavorites(Account customer, Book book);
    public List<Book> getFavorites(Account customer);
//    public BookEntity isBookInFavorites(Account account, Book book);

}
