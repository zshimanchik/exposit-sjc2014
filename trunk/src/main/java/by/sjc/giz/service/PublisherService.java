package by.sjc.giz.service;

import by.sjc.giz.model.Book;
import by.sjc.giz.model.BooksFilter;
import by.sjc.giz.model.Order;
import by.sjc.giz.model.Subject;
import by.sjc.giz.model.publisher.BooksWithMaxCount;
import by.sjc.giz.model.publisher.ContractWithAuthors;
import by.sjc.giz.model.publisher.Discount;

import java.util.List;

/**
 * Created by irina on 29.07.14.
 */
public interface PublisherService {
    public List<Subject> getPublisherList();
    public List<ContractWithAuthors> getContractWithAuthorsList(int publisherId);
    public List<Book> getPublisherBookList(int publisherId);

    public void addContractWithAuthors(ContractWithAuthors contract);

    public List<Discount> getDiscountList(Subject publisher, int pageSize, int pageNumber);
    public void changeDiscount(Subject publisher, Subject client, int value);

    public BooksWithMaxCount getBooks(int pageSize, int pageNumber, BooksFilter filter);
}
