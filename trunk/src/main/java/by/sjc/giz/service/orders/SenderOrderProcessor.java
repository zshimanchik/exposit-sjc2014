package by.sjc.giz.service.orders;

import by.sjc.giz.dao.entity.OrderEntity;
import by.sjc.giz.model.Order;

/**
 * Created by Z on 18.08.14.
 * Interface for Processor, which would handle orders actions from sender-side, e.g. cancel order, confirm order.
 */
public interface SenderOrderProcessor {
    public boolean confirmOutgoingOrder(Order order);
}
