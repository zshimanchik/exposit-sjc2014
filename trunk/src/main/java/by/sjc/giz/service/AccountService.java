package by.sjc.giz.service;

import java.util.List;

import by.sjc.giz.model.Account;
import by.sjc.giz.model.Post;

/**
 * Created by irina on 29.07.14.
 */
public interface AccountService {

    public List<Account> getAccounts();
    public Account getAccountById(int id);
    public Account getAccountByEmail(String email);
    public Account loadAccountByCredentials(String email, String password);
    public Account registerAccount(Account account);
    public Account registerCustomerAccount(Account account);

    public List<Account> getSubjectAccounts(int subjectId);
    
    public float getFundsSpentByAccount(Account account);
    
    public void addPost(Post post);
}
