package by.sjc.giz.service.impl;

import by.sjc.giz.dao.converters.SubjectConverter;
import by.sjc.giz.dao.entity.SubjectEntity;
import by.sjc.giz.dao.repositiory.SubjectDao;
import by.sjc.giz.model.Subject;
import by.sjc.giz.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Z on 11.08.14.
 */
@Service
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    private SubjectDao subjectDao;

    @Autowired
    private SubjectConverter subjectConverter;

    @Override
    public Subject getSubjectById(int subjectId) {
        SubjectEntity subjectEntity = subjectDao.getSubjectById(subjectId);
        return subjectConverter.convertToSubject(subjectEntity);
    }
}
