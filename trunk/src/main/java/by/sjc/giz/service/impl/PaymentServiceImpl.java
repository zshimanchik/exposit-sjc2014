package by.sjc.giz.service.impl;

import by.sjc.giz.model.payment.PaymentProcessor;
import by.sjc.giz.model.payment.Payment;
import by.sjc.giz.payment.PaymentProcessorFactoryImpl;
import by.sjc.giz.model.payment.PaymentStatus;
import by.sjc.giz.service.PaymentService;
import org.springframework.stereotype.Service;

/**
 * Created by Z on 29.07.14.
 */
@Service
public class PaymentServiceImpl implements PaymentService {
    @Override
    public void paymentRequest(Payment payment, float value) {
        PaymentProcessor paymentProcessor = PaymentProcessorFactoryImpl.getInstance().getPaymentProcessor(payment);
        paymentProcessor.paymentRequest(payment, value);
    }

    @Override
    public PaymentStatus checkPaymentStatus(Payment payment) {
        PaymentProcessor paymentProcessor = PaymentProcessorFactoryImpl.getInstance().getPaymentProcessor(payment);
        return paymentProcessor.checkPaymentStatus(payment);
    }
}
