package by.sjc.giz.service;

import by.sjc.giz.model.Account;
import by.sjc.giz.model.Cart;
import by.sjc.giz.model.Order;
import by.sjc.giz.model.payment.Payment;
import by.sjc.giz.model.shop.BookInShopListWrapper;

import java.util.List;

/**
 * Created by irina on 31.07.14.
 */
public interface OrderService {

    public List<Order> getAllOrders(); //for debug. remove later
    public List<Order> getOutgoingOrders(int senderId);
    public List<Order> getIncomingOrders(int recipientId);

    public List<Order> getCompletedOutgoingOrders(int senderId);
    public List<Order> getCompletedIncomingOrders(int recipientId);

    public List<Order> getSatisfiedOutgoingOrders(int senderId);
    public List<Order> getSatisfiedIncomingOrders(int recipientId);

    //Order Service for sender
    public Order confirmOutgoingOrder(int orderId);
    public Order cancelOutgoingOrder(int orderId);
    public Order[] cancelBookFromOutgoingOrder(int orderId, int bookId);

    //Order Service for recipient
    public void addOrder(Order order);
    public void makeOrder(Account account, Payment payment, String address);
    public BookInShopListWrapper makeOrderFromShopToPublisher(Account account, Payment payment, String address);
    public Order confirmIncomingOrder(int orderId);
    public Order rejectIncomingOrder(int orderId);
    public Order[] rejectBookFromIncomingOrder(int orderId, int bookId);

    public Order getById(int orderId);
    public float getPrice(Cart cart);
}
