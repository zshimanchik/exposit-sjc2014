package by.sjc.giz.service.orders;

import by.sjc.giz.model.Order;

/**
 * Created by Z on 18.08.14.
 * Interface for Processor, which would handle orders actions from recipient-side, e.g. reject order, confirm order.
 */
public interface RecipientOrderProcessor {

    public void addOrder(Order order);
    public boolean confirmIncomingOrder(Order order);

}
