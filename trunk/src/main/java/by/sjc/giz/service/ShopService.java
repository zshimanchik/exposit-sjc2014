package by.sjc.giz.service;

import by.sjc.giz.model.Book;
import by.sjc.giz.model.BooksFilter;
import by.sjc.giz.model.OrderRecord;
import by.sjc.giz.model.Subject;
import by.sjc.giz.model.shop.BookInShop;
import by.sjc.giz.model.shop.BooksWithMaxCount;

import java.util.Date;
import java.util.List;

/**
 * Created by irina on 29.07.14.
 */
public interface ShopService {

    public List<Subject> shops();
    public void addBookToStore(int shopId, Book book, int count, int price);
    public void addBookToStore(BookInShop bookInShop);
    public void addBooksToStore(List<BookInShop> books);
    public void addBooksToStore(int shopId, List<OrderRecord> orderBooks);
    public List<String> getProfitableGenres(int shopId);
    public boolean canSatisfyOrder(int shopId, List<OrderRecord> orderBooks);
    public void removeFromStore(int shopId, List<OrderRecord> orderBooks);
    public List<BookInShop> getBooks(int shopId);
    public BooksWithMaxCount getBooks(int pageSize, int pageNumber, BooksFilter filter);

    public void updateBookInShop(BookInShop bookInShop);
    public BookInShop getBookInShop(int bookId, int shopId);
    //profit
    public float getProfit(int shopId, Date startDate, Date endDate);

    public Date getDateOfFirstOrder(int shopId);
    public Date getDateOfLastOrder(int shopId);

    public float getBookPrice(int shopId, int bookId);
}
