package by.sjc.giz.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import by.sjc.giz.dao.converters.AccountConverter;
import by.sjc.giz.dao.converters.BookConverter;
import by.sjc.giz.dao.entity.AccountEntity;
import by.sjc.giz.dao.entity.OrderEntity;
import by.sjc.giz.dao.entity.OrderRecordEntity;
import by.sjc.giz.dao.entity.PostEntity;
import by.sjc.giz.dao.repositiory.AccountDao;
import by.sjc.giz.dao.repositiory.BookDao;
import by.sjc.giz.dao.repositiory.OrderDao;
import by.sjc.giz.model.Account;
import by.sjc.giz.model.Post;
import by.sjc.giz.model.Subject;
import by.sjc.giz.model.customer.Customer;
import by.sjc.giz.service.AccountService;

/**
 * Created by Z on 07.08.14.
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    public static final Logger logger = Logger.getLogger(AccountService.class);

    @Autowired
    private AccountDao accountDao;
    @Autowired
    private AccountConverter accountConverter;
    @Autowired
    private OrderDao orderDao;
    @Autowired
    private BookDao bookDao;
    @Autowired
    private BookConverter bookConverter;

    @Override
    public List<Account> getAccounts() {
        return null;
    }

    @Override
    public Account getAccountById(int id) {
        return null;
    }

    @Override
    public Account getAccountByEmail(String email) {
        AccountEntity accountEntity = accountDao.loadAccountByEmail(email);
        if (accountEntity==null)
            return null;

        Account account = accountConverter.convertToAccount(accountEntity);
        return account;
    }

    @Override
    public Account loadAccountByCredentials(String email, String password) {
        AccountEntity accountEntity = accountDao.loadAccountByCredentials(email,password);

        //Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        Account account = null;
        if (accountEntity != null) {
            //account = mapper.map(accountEntity, Account.class);
            account = accountConverter.convertToAccount(accountEntity);
            logger.info(":::::::::entity:" + accountEntity.getId() + " | " + accountEntity.getEmail() + " | "+ accountEntity.getSubject().toString());
            logger.info("::::::::::model:" + account.getFirstName()+" " + account.getLastName() );
        }
        return account;
    }

    @Override
    public Account registerAccount(Account account) {
        return account;
    }

    @Override
    public Account registerCustomerAccount(Account account) {
        Subject subject = new Customer( account.getFirstName() + " " + account.getLastName() );
        account.setSubject(subject);
        AccountEntity accountEntity = accountConverter.convertToAccountEntity(account);

        accountDao.registerAccount(accountEntity);
        return this.getAccountByEmail( accountEntity.getEmail() );
    }

    @Override
    public List<Account> getSubjectAccounts(int subjectId) {
        List<AccountEntity> accountEntityList = accountDao.getSubjectAccounts(subjectId);
        List<Account> accountList = new ArrayList<Account>( accountEntityList.size() );

        for (AccountEntity accountEntity : accountEntityList) {
            Account account = accountConverter.convertToAccount(accountEntity);
            accountList.add(account);
        }

        return accountList;
    }
	
	public float getFundsSpentByAccount(Account account){
		float sum = 0.0f;
		
		AccountEntity accountEntity = accountConverter.convertToAccountEntity(account);
		//emm, accEntity's id is equal to acc's id - maybe shouldn't make this convertation
		
		List<OrderEntity> satisfiedOutgoingOrders = orderDao.
				getSatisfiedOutgoingOrders(accountEntity.getSubject().getId());
		for (OrderEntity orderEntity : satisfiedOutgoingOrders) {
			List<OrderRecordEntity> orderRecords = orderEntity.getRecords();
			for (OrderRecordEntity orderRecord : orderRecords) {
				sum+=( orderRecord.getBookPrice() * orderRecord.getCount() );
			}
		}
		return sum;
	}

	@Override
	public void addPost(Post post) {
		PostEntity postEntity = bookConverter.convertToPostEntity(post);
		bookDao.addPost(postEntity);		
	}
}
