package by.sjc.giz.service;

import by.sjc.giz.model.Book;
import by.sjc.giz.model.Post;
import by.sjc.giz.model.Subject;

import java.util.List;

/**
 * Created by Z on 17.08.14.
 */
public interface BookService {
    public Book getBookById(int bookId);

    public List<String> getAllGenres();
    public List<String> getAllAuthors();
    public List<String> getAllPublishers();
    public List<String> getAllShops();

    public boolean addGenre(String genreName);
    public boolean addAuthor(String authorName);

    public float getBookPrice(Subject client, Subject seller, int bookId);

    public List<Post> getBookPosts(int bookId);
}
