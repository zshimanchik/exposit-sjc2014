package by.sjc.giz.service.orders;

import by.sjc.giz.model.Subject;
import by.sjc.giz.model.SubjectType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Z on 18.08.14.
 */
@Component
public class OrderProcessorFactory {
    @Autowired
    private CustomerOrderProcessor customerOrderProcessor;
    @Autowired
    private ShopOrderProcessor shopOrderProcessor;
    @Autowired
    private PublisherOrderProcessor publisherOrderProcessor;

    public SenderOrderProcessor getSenderOrderProcessor(Subject sender) {
        switch (sender.getType()) {
            case CUSTOMER:
                return customerOrderProcessor;
            case SHOP:
                return shopOrderProcessor;
        }
        return null;
    }

    public RecipientOrderProcessor getRecipientOrderProcessor(Subject recipient) {
        switch (recipient.getType()) {
            case SHOP:
                return shopOrderProcessor;
            case PUBLISHER:
                return publisherOrderProcessor;
        }
        return null;
    }

}
