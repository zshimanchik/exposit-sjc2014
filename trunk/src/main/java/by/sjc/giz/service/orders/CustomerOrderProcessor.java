package by.sjc.giz.service.orders;

import by.sjc.giz.dao.converters.OrderConverter;
import by.sjc.giz.dao.entity.OrderEntity;
import by.sjc.giz.dao.repositiory.OrderDao;
import by.sjc.giz.model.Order;
import by.sjc.giz.model.OrderStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by Z on 18.08.14.
 */
@Component
public class CustomerOrderProcessor implements SenderOrderProcessor {

    public static final Logger logger = Logger.getLogger(CustomerOrderProcessor.class);

    @Autowired
    private OrderConverter orderConverter;
    @Autowired
    private  OrderDao orderDao;

    @Override
    public boolean confirmOutgoingOrder(Order order) {
        switch (order.getState()) {
            case PAYED: {
                order.setState(OrderStatus.SATISFIED);
                break;
            }
            case RECIPIENT_REJECTED: {
                order.setState(OrderStatus.ABORTIVE);
                break;
            }
            default:
                return false;
        }//switch

        OrderEntity orderEntity = orderConverter.convertToOrderEntity(order);
        orderEntity.setChangeDate(new Date());
        orderDao.saveOrUpdate(orderEntity);
        return true;
    }
}
