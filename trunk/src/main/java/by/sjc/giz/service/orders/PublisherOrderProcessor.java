package by.sjc.giz.service.orders;

import by.sjc.giz.dao.converters.OrderConverter;
import by.sjc.giz.dao.entity.OrderEntity;
import by.sjc.giz.dao.repositiory.OrderDao;
import by.sjc.giz.model.Order;
import by.sjc.giz.model.OrderStatus;
import by.sjc.giz.service.PaymentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by Z on 18.08.14.
 * Order processor for publisher to perform actions
 */
@Component
public class PublisherOrderProcessor implements RecipientOrderProcessor {

    public static final Logger logger = Logger.getLogger(PublisherOrderProcessor.class);

    @Autowired
    private PaymentService paymentService;
    @Autowired
    private OrderConverter orderConverter;
    @Autowired
    private OrderDao orderDao;

    @Override
    public void addOrder(Order order) {
        OrderEntity orderEntity = orderConverter.convertToOrderEntity(order);
        orderDao.save(orderEntity);
    }

    @Override
    public boolean confirmIncomingOrder(Order order) {
        switch (order.getState()) {
            case NEW : {
                if ( ! handleNewOrder(order) )
                    return false;
                break;
            }
            case SENDER_CANCELED: {
                if ( ! handleSenderCanceledOrder(order))
                    return false;
                break;
            }
            case WAITING_FOR_BOOK_RELEASES: {
                if ( ! handleWaitingForBookReleasesOrder(order))
                    return false;
                break;
            }
            case WAITING_FOR_PAY: {
                if ( ! handleWaitingForPayOrder(order))
                    return false;
                break;
            }
            default: return false;
        }
        OrderEntity orderEntity = orderConverter.convertToOrderEntity(order);
        orderEntity.setChangeDate(new Date());
        logger.info("updating order="+orderEntity);
        orderDao.saveOrUpdate(orderEntity);
        return true;
    }



    private boolean handleNewOrder(Order order) {
        if (order.getIsPreorder()) {
            order.setState(OrderStatus.WAITING_FOR_BOOK_RELEASES);
        } else  {
            handleWaitingForPayOrder(order);
        }
        return true;
    }

    private boolean handleSenderCanceledOrder(Order order) {
        order.setState(OrderStatus.ABORTIVE);
        return true;
    }

    private boolean handleWaitingForBookReleasesOrder(Order order) {
        if ( order.getIsPreorder() ) {
            return false;
        } else {
            //order is released. so requesting payment
            //requesting payment
            paymentService.paymentRequest(order.getPayment(), order.getCost());
            //and checking
            handleWaitingForPayOrder(order);
        }
        return true;
    }

    private boolean handleWaitingForPayOrder(Order order) {
        switch (paymentService.checkPaymentStatus(order.getPayment())) {
            case DENIED:
                order.setState(OrderStatus.RECIPIENT_REJECTED);
                break;
            case PAYED:
                order.setState(OrderStatus.PAYED);
                break;
            case WAITING_FOR_USER:
                order.setState(OrderStatus.WAITING_FOR_PAY);
                return false;
        }
        return true;
    }
}
