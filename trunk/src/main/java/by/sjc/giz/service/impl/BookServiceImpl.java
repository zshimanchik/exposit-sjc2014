package by.sjc.giz.service.impl;

import by.sjc.giz.dao.converters.BookConverter;
import by.sjc.giz.dao.converters.SubjectConverter;
import by.sjc.giz.dao.entity.AuthorEntity;
import by.sjc.giz.dao.entity.BookEntity;
import by.sjc.giz.dao.entity.GenreEntity;
import by.sjc.giz.dao.entity.PostEntity;
import by.sjc.giz.dao.entity.SubjectEntity;
import by.sjc.giz.dao.entity.publisher.DiscountEntity;
import by.sjc.giz.dao.repositiory.BookDao;
import by.sjc.giz.dao.repositiory.PublisherDao;
import by.sjc.giz.dao.repositiory.ShopDao;
import by.sjc.giz.model.Book;
import by.sjc.giz.model.Post;
import by.sjc.giz.model.Subject;
import by.sjc.giz.service.BookService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Z on 17.08.14.
 */
@Service
public class BookServiceImpl implements BookService {

    public static final Logger logger = Logger.getLogger(BookService.class);

    @Autowired
    private BookDao bookDao;
    @Autowired
    private ShopDao shopDao;
    @Autowired
    private PublisherDao publisherDao;

    @Autowired
    private BookConverter bookConverter;

    @Override
    public Book getBookById(int bookId) {
        BookEntity bookEntity = bookDao.getBookById(bookId);
        return bookConverter.convertToBook(bookEntity);
    }

    @Override
    public List<String> getAllGenres() {
        List<GenreEntity> genreEntities = bookDao.getAllGenreEntities();
        List<String> genres = new ArrayList<String>( genreEntities.size() );
        for (GenreEntity genre : genreEntities) {
            genres.add(genre.getName());
        }
        return genres;
    }

    @Override
    public List<String> getAllAuthors() {
        List<AuthorEntity> authorEntities = bookDao.getAllAuthorEntities();
        List<String> authors = new ArrayList<String>( authorEntities.size() );
        for (AuthorEntity author : authorEntities) {
            authors.add(author.getName());
        }
        return authors;
    }

    @Override
    public List<String> getAllPublishers() {
        List<SubjectEntity> publisherEntities = publisherDao.getPublisherList();
        List<String> publishers = new ArrayList<String>( publisherEntities.size() );
        for (SubjectEntity publisherEntity : publisherEntities)
            publishers.add(publisherEntity.getName());
        return publishers;
    }

    @Override
    public List<String> getAllShops() {
        List<SubjectEntity> shopEntities = shopDao.getShopList();
        List<String> shops = new ArrayList<String>( shopEntities.size() );
        for (SubjectEntity shopEntity : shopEntities)
            shops.add(shopEntity.getName());
        return shops;
    }

    @Override
    public boolean addGenre(String genreName) {
        if ( bookDao.getGenreEntity(genreName) == null) {
            GenreEntity genreEntity = new GenreEntity();
            genreEntity.setName(genreName);
            logger.info("add new genre=" + genreName);
            bookDao.addGenre(genreEntity);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean addAuthor(String authorName) {
        if ( bookDao.getAuthorEntity(authorName) == null) {
            AuthorEntity authorEntity = new AuthorEntity();
            authorEntity.setName(authorName);
            logger.info("add new author=" + authorName);
            bookDao.addAuthor(authorEntity);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public float getBookPrice(Subject client, Subject seller, int bookId) {
        switch (seller.getType()) {
            case SHOP: {
                return shopDao.getBookPrice(seller.getId(),bookId);
            }
            case PUBLISHER: {
                float price = publisherDao.getBookPrice(bookId);
                //checking discount
                DiscountEntity discount = publisherDao.getDiscount(seller.getId(), client.getId());
                if (discount!=null)
                    price *= 1.0f - discount.getValue() / 100.0f;
                return price;
            }
            default:
                throw new RuntimeException("wrong seller subject"+seller);
        }
    }
    
	@Override
	public List<Post> getBookPosts(int bookId) {
		List<PostEntity> postEntities = bookDao.getBookPosts(bookId);
        List<Post> posts = new ArrayList<Post>( postEntities.size() );
        for (PostEntity postEntity : postEntities) {
            posts.add(bookConverter.convertToPost(postEntity));
        }
        return posts;
	}
}
