package by.sjc.giz.service;

import by.sjc.giz.model.payment.Payment;
import by.sjc.giz.model.payment.PaymentStatus;


/**
 * Created by Z on 29.07.14.
 */
public interface PaymentService {
    public void paymentRequest(Payment payment, float value);
    public PaymentStatus checkPaymentStatus(Payment payment);
}
