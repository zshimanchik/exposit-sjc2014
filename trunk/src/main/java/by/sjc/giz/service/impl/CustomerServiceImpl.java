package by.sjc.giz.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.sjc.giz.dao.converters.AccountConverter;
import by.sjc.giz.dao.converters.BookConverter;
import by.sjc.giz.dao.entity.AccountEntity;
import by.sjc.giz.dao.entity.BookEntity;
import by.sjc.giz.dao.repositiory.AccountDao;
import by.sjc.giz.dao.repositiory.BookDao;
import by.sjc.giz.model.Account;
import by.sjc.giz.model.Book;
import by.sjc.giz.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private AccountDao accountDao;
	@Autowired
	private BookConverter bookConverter;


	@Override
	public void addToFavorites(Account customer, Book book) {
		BookEntity bookEntity = bookConverter.convertToBookEntity(book);
		AccountEntity accountEntity = accountDao.loadAccountById(customer.getId());
		boolean flag = false;
		for (BookEntity tmpBook : accountEntity.getFavoriteBookEntitiesSet()) {
			if(tmpBook.getId()==bookEntity.getId())flag=true;			
		}
		if(!flag){
			accountEntity.getFavoriteBookEntitiesSet().add(bookEntity);	
			accountDao.updateAccount(accountEntity);			
		}	
//		BookEntity tmpBook = isBookInFavorites(customer, book);
//		if(tmpBook==null){
//			accountEntity.getFavoriteBookEntitiesSet().add(bookEntity);	
//			accountDao.updateAccount(accountEntity);			
//		}
//		if(!accountEntity.getFavoriteBookEntitiesSet().contains(bookEntity)){
//			accountEntity.getFavoriteBookEntitiesSet().add(bookEntity);	
//			accountDao.updateAccount(accountEntity);	
//		}
	}

	@Override
	public void removeFromFavorites(Account customer, Book targetBook) {
		BookEntity bookEntity = bookConverter.convertToBookEntity(targetBook);
		AccountEntity accountEntity = accountDao.loadAccountById(customer.getId());
//		BookEntity tmpBook = isBookInFavorites(customer, targetBook);
//		if(tmpBook!=null){
//			accountEntity.getFavoriteBookEntitiesSet().remove(tmpBook);
//			accountDao.updateAccount(accountEntity);
//		}
		for (BookEntity book : accountEntity.getFavoriteBookEntitiesSet()) {
			if(book.getId()==bookEntity.getId()){
				accountEntity.getFavoriteBookEntitiesSet().remove(book);
				accountDao.updateAccount(accountEntity);
				break;
			}
		}
//		if(accountEntity.getFavoriteBookEntitiesSet().contains(bookEntity)){
//			accountEntity.getFavoriteBookEntitiesSet().remove(bookEntity);
//			accountDao.updateAccount(accountEntity);
//		}
	}
	
	@Override
	public List<Book> getFavorites(Account customer){
		AccountEntity accountEntity = accountDao.loadAccountById(customer.getId());
		Set<BookEntity> bookEntities = accountEntity.getFavoriteBookEntitiesSet();
		List<Book> books = new ArrayList<Book>(bookEntities.size());
		for (BookEntity bookEntity : bookEntities) {
			books.add(bookConverter.convertToBook(bookEntity));
		}
		return books;
	}

//	@Override
//	public BookEntity isBookInFavorites(Account account, Book book) {
//		AccountEntity accountEntity = accountDao.loadAccountById(account.getId());
//		Set<BookEntity> bookEntities = accountEntity.getFavoriteBookEntitiesSet();
//		for (BookEntity book2 : bookEntities) {
//			if(book2.getId()==book.getId())return book2;
//		}
//		return null;
//	}

}
