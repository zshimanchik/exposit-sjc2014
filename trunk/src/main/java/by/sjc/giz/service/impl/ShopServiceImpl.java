package by.sjc.giz.service.impl;

import by.sjc.giz.dao.converters.BookConverter;
import by.sjc.giz.dao.converters.SubjectConverter;
import by.sjc.giz.dao.entity.SubjectEntity;
import by.sjc.giz.dao.entity.shop.BookInShopEntity;
import by.sjc.giz.dao.repositiory.ShopDao;
import by.sjc.giz.model.Book;
import by.sjc.giz.model.BooksFilter;
import by.sjc.giz.model.OrderRecord;
import by.sjc.giz.model.Subject;
import by.sjc.giz.model.shop.BookInShop;
import by.sjc.giz.model.shop.BooksWithMaxCount;
import by.sjc.giz.service.ShopService;
import by.sjc.giz.service.SubjectService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by irina on 10.08.14.
 */

@Service
public class ShopServiceImpl implements ShopService {

    public static final Logger logger = Logger.getLogger(ShopService.class);

    @Autowired
    private ShopDao shopDao;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private SubjectConverter subjectConverter;
    @Autowired
    private BookConverter bookConverter;

    @Override
    public List<Subject> shops() {
        List<SubjectEntity> subjectEntityList = shopDao.getShopList();
        List<Subject> shopList = subjectConverter.convertToSubject(subjectEntityList);
        logger.debug("shopList="+shopList);
        return shopList;
    }

    @Override
    public void addBookToStore(int shopId, Book book, int count, int price) {
        //TODO I. see publisherDao.getBookCost(bookId) to deriving sell price
    }

    @Override
    public void addBookToStore(BookInShop bookInShop) {
        BookInShopEntity bookInShopEntity = bookConverter.convertToBookInShopEntity(bookInShop);
        shopDao.addBookToStore(bookInShopEntity);
        logger.info("ADD BOOK TO STORE: " + bookInShop.getBook().getName());
    }

    @Override
    public void addBooksToStore(List<BookInShop> books) {
        for (BookInShop bookInShop : books) {
            addBookToStore(bookInShop);
        }
    }

    @Override
    public void addBooksToStore(int shopId, List<OrderRecord> orderBooks) {
        for (OrderRecord orderRecord: orderBooks) {
            BookInShop bookInShop = new BookInShop();
            bookInShop.setBook(orderRecord.getBook());
            bookInShop.setSellPrice(0);
            bookInShop.setCount(orderRecord.getCount());
            bookInShop.setShop(subjectService.getSubjectById(shopId));
            addBookToStore(bookInShop);
        }
    }


    @Override
    public List<String> getProfitableGenres(int shopId) {
        return shopDao.getProfitableGenres(shopId);
    }

    @Override
    public boolean canSatisfyOrder(int shopId, List<OrderRecord> orderBooks) {
        for (OrderRecord orderRecord : orderBooks) {
            int cnt = shopDao.getBookCount(shopId, orderRecord.getBook().getId());
            if ( cnt < orderRecord.getCount() )
                return false;
        }
        return true;
    }

    @Override
    public void removeFromStore(int shopId, List<OrderRecord> orderBooks) {
        for (OrderRecord orderRecord : orderBooks) {
            BookInShopEntity bookEntity = shopDao.getBookInShopById(shopId, orderRecord.getBook().getId());
            bookEntity.setCount(bookEntity.getCount() - orderRecord.getCount());
            shopDao.updateBookInShop(bookEntity);
        }
    }

    @Override
    public List<BookInShop> getBooks(int shopId) {
        List<BookInShopEntity> bookInShopEntityList = shopDao.getBookList(shopId);
        return bookConverter.convertToBookInShop(bookInShopEntityList);
    }

    @Override
    public BooksWithMaxCount getBooks(int pageSize, int pageNumber, BooksFilter filter) {
        List<BookInShopEntity> bookInShopEntities = shopDao.getBooks(pageSize, pageNumber, filter);
        int maxCount = 0;
        if (pageSize>0)
            maxCount = shopDao.getBooksCount(filter);

        BooksWithMaxCount res = new BooksWithMaxCount();
        res.setBooks( bookConverter.convertToBookInShop(bookInShopEntities) );
        res.setMaxCount(maxCount);
        return res;
    }

    @Override
    public void updateBookInShop(BookInShop bookInShop) {
        BookInShopEntity bookInShopEntity = bookConverter.convertToBookInShopEntity(bookInShop);
        shopDao.updateBookInShop(bookInShopEntity);
    }

    @Override
    public BookInShop getBookInShop(int bookId, int shopId) {
        BookInShopEntity bookInShopEntity = shopDao.getBookInShopById(shopId, bookId);
        return bookConverter.convertToBookInShop(bookInShopEntity);
    }

    @Override
    public float getProfit(int shopId, Date startDate, Date endDate) {
        return shopDao.getProfit(shopId, startDate, endDate);
    }

    @Override
    public Date getDateOfFirstOrder(int shopId) {
        return shopDao.getDateOfFirstOrder(shopId);
    }

    @Override
    public Date getDateOfLastOrder(int shopId) {
        return shopDao.getDateOfLastOrder(shopId);
    }

    @Override
    public float getBookPrice(int shopId, int bookId) {
        return shopDao.getBookPrice(shopId, bookId);
    }
}
