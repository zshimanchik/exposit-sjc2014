package by.sjc.giz.service.impl;

import by.sjc.giz.dao.converters.*;
import by.sjc.giz.dao.entity.BookEntity;
import by.sjc.giz.dao.entity.OrderEntity;
import by.sjc.giz.dao.entity.SubjectEntity;
import by.sjc.giz.dao.entity.publisher.ContractWithAuthorsEntity;
import by.sjc.giz.dao.entity.publisher.DiscountEntity;
import by.sjc.giz.dao.repositiory.OrderDao;
import by.sjc.giz.dao.repositiory.PublisherDao;
import by.sjc.giz.model.*;
import by.sjc.giz.model.publisher.BooksWithMaxCount;
import by.sjc.giz.model.publisher.ContractWithAuthors;
import by.sjc.giz.model.publisher.Discount;
import by.sjc.giz.service.PaymentService;
import by.sjc.giz.service.PublisherService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by Z on 11.08.14.
 */
@Service
public class PublisherServiceImpl implements PublisherService {

    public static final Logger logger = Logger.getLogger(PublisherServiceImpl.class);

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PublisherDao publisherDao;
    @Autowired
    private OrderDao orderDao;

    @Autowired
    private OrderConverter orderConverter;
    @Autowired
    private SubjectConverter subjectConverter;
    @Autowired
    private ContractWithAuthorsConverter contractWithAuthorsConverter;
    @Autowired
    private BookConverter bookConverter;
    @Autowired
    private DiscountConverter discountConverter;



    @Override
    public List<Subject> getPublisherList() {
        List<SubjectEntity> subjectEntityList = publisherDao.getPublisherList();
        List<Subject> publisherList = subjectConverter.convertToSubject(subjectEntityList);
        return publisherList;
    }


    @Override
    public List<Book> getPublisherBookList(int publisherId) {
        List<BookEntity> bookEntityList = publisherDao.getPublisherBookList(publisherId);
        List<Book> bookList = bookConverter.convertToBook(bookEntityList);
        return bookList;
    }

    @Override
    public List<ContractWithAuthors> getContractWithAuthorsList(int publisherId) {
        List<ContractWithAuthorsEntity> contractEntityList =
                        publisherDao.getContractWithAuthorsList(publisherId);
        List<ContractWithAuthors> contractList =
                        contractWithAuthorsConverter.convertToContractWithAuthors(contractEntityList);
        return contractList;
    }

    @Override
    public void changeDiscount(Subject publisher, Subject client, int value) {
        DiscountEntity discountEntity = publisherDao.getDiscount(publisher.getId(), client.getId());
        if (discountEntity==null) {
            Discount discount = new Discount(0, publisher, client,value);
            discountEntity = discountConverter.convertToDiscountEntity(discount);
        } else {
            discountEntity.setValue(value);
        }
        logger.info("save or update discount="+discountEntity);
        publisherDao.saveOrUpdateDiscountEntity(discountEntity);
    }

    @Override
    public List<Discount> getDiscountList(Subject publisher, int pageSize, int pageNumber) {
        List<DiscountEntity> discountEntities = publisherDao.getDiscountsList(publisher.getId(), pageSize, pageNumber);
        List<Discount> discounts = discountConverter.convertToDiscount(discountEntities);
        logger.info("discounts for "+publisher.getName() +" \n "+discounts);
        return discounts;
    }

    @Override
    public void addContractWithAuthors(ContractWithAuthors contract) {
        ContractWithAuthorsEntity contractEntity =
                    contractWithAuthorsConverter.convertToContractWithAuthorsEntity(contract);
        publisherDao.addContractWithAuthors(contractEntity);
    }

    @Override
    public BooksWithMaxCount getBooks(int pageSize, int pageNumber, BooksFilter filter) {
        List<ContractWithAuthorsEntity> contractEntities = publisherDao.getContracts(pageSize, pageNumber, filter);
        int maxCount = 0;
        if (pageSize>0)
            maxCount = publisherDao.getContractsCount(filter);

        BooksWithMaxCount res = new BooksWithMaxCount();
        res.setContracts( contractWithAuthorsConverter.convertToContractWithAuthors( contractEntities ) );
        res.setMaxCount(maxCount);
        return res;
    }
}
